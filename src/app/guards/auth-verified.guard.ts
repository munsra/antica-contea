import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthVerifiedGuard implements CanActivate {
  constructor(private afAuth: AngularFireAuth, public router: Router, private alertController: AlertController){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return new Promise(resolve => {
        this.afAuth.user.subscribe(user => {
          if (user) {
            user.reload().then(x => {
              if(user.emailVerified) {
                console.log("##### User Guard: auth = true");
                resolve(true);     
              } else {
                console.log("##### User Guard: Email is not verified");
                this.presentAlertConfirm();
                resolve(false);
              }
            })
            
          } else {
            console.log("##### User Guard: auth = false");
            console.log('User is not logged in');
            //this.router.navigate(['/login']);
            this.router.navigate(['fire-signin'], {
              queryParams: {
                redirectUrl: state.url
              }
            });
            // route with redirect http://gnomeontherun.com/2017/03/02/guards-and-login-redirects-in-angular/
            //this.router.navigate(['/product-list'], { queryParams: { redirect: 3 }});
            resolve(false);
          }
        });
      });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'No permission',
      message: 'Devi verificare la tua email! Se hai già verificato l\'email, fai LOGOUT.',
      buttons: [
        {
          text: 'Annulla',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Verifica Email',
          handler: () => {
            console.log('Go to verify page');
            
            this.router.navigateByUrl('tabs/user-profile/verify-email');
          }
        }
      ]
    });
  
    await alert.present();
  }
}
