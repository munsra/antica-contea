import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { Observable } from 'rxjs';
import { BeerService } from 'src/app/services/beer.service';

@Component({
  selector: 'app-beer-detail',
  templateUrl: './beer-detail.page.html',
  styleUrls: ['./beer-detail.page.scss'],
})
export class BeerDetailPage implements OnInit {

  parentPath:any;


  //**** toolbar for hide and show ****/
  showToolbar = false;
  showColor = false;
  showTitle = false;
  transition:boolean = false;
  quantity: number = 1;
  item: Observable<any>;
  itemArray: any=[];
  itemOptions: any=[];
  itemSubscribe: any;
  // relatedPlacesArray: any=[];
  // reviews: Observable<any[]>;
  itemId: any;
  categoryId: any;
  showAttribute1: boolean;
  showAttribute2: boolean;
  hasSomeSizeEnabled: boolean = false;

  //**** Size and Addon  ****//
  selectSize: any;  
  
   // ******** for Cart ***********//
   cart = [];

  //**** User authentication  ****//
  
  userAuth: boolean = false; // Is user logged in ?
  userId: any;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private beerService: BeerService,
    private modalController: ModalController,
    public router: Router,
  ) { 
    this.itemId = this.activatedRoute.snapshot.paramMap.get('itemId');
    this.cart = this.beerService.getCart();
  }

  async ngOnInit() {
    this.item =  await this.beerService.getItemDetail(this.itemId);
    this.itemSubscribe = this.item.subscribe(res => {
      this.itemArray = res;
      this.selectSize = this.itemArray.attribute1_value[0];
      this.itemArray.attribute1_value.forEach(element => {
        if(element.enable){
          this.hasSomeSizeEnabled = true;
        }
      });
    });
 
  }
  
  ngOnDestroy() {
		this.itemSubscribe.unsubscribe()
  }

  closeModal() {
    this.modalController.dismiss();
  }

  addToCart() {
    const addItem = {
      //id :this.itemId, 
      itemId:this.itemId,
      name: this.itemArray.name,
      price: this.itemArray.price,
      description: this.itemArray.description,
      size: this.selectSize,
      image: this.itemArray.image,
      quantity: this.quantity,
      //image: this.image
      }
      // console.log(this.addOnItem);
      // console.log("select SIZE="+ this.selectSize);
      
    this.beerService.addToCart(addItem);
    console.log("______addItem="+ JSON.stringify(addItem) );
    this.router.navigateByUrl('/cart-checkout');
    
  }

  onScroll($event: CustomEvent) {
    if ($event && $event.detail && $event.detail.scrollTop) {
      const scrollTop = $event.detail.scrollTop;
      this.transition = true;
      this.showToolbar = scrollTop >= 50;
      //console.log("showToolbar="+this.showToolbar);
      this.showTitle = scrollTop >= 50;
      //console.log("showTitle="+this.showTitle);
    }else{
      this.transition = false;
    }
  }

  itemValueChange(){
    if(this.itemArray && this.selectSize){
      this.itemArray.price = this.selectSize.price * this.quantity;
    }
  }

  get itemPrice(){
    if(this.itemArray && this.selectSize){
      return this.itemArray.price = this.selectSize.price * this.quantity;
    } 
    return this.itemArray.price = 0;
  }

}
