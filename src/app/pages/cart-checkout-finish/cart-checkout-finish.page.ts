import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { BeerService } from 'src/app/services/beer.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-cart-checkout-finish',
  templateUrl: './cart-checkout-finish.page.html',
  styleUrls: ['./cart-checkout-finish.page.scss'],
})
export class CartCheckoutFinishPage implements OnInit {

  constructor(    

    public beerService: BeerService,
    public userService: UserService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    public router: Router,
) {
    this.beerService.removeAllItemCart();
   }

  ngOnInit() {
  }

}
