import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartCheckoutFinishPageRoutingModule } from './cart-checkout-finish-routing.module';

import { CartCheckoutFinishPage } from './cart-checkout-finish.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartCheckoutFinishPageRoutingModule,
    TranslateModule
  ],
  declarations: [CartCheckoutFinishPage]
})
export class CartCheckoutFinishPageModule {}
