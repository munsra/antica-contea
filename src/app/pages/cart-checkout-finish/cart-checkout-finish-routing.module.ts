import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CartCheckoutFinishPage } from './cart-checkout-finish.page';

const routes: Routes = [
  {
    path: '',
    component: CartCheckoutFinishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CartCheckoutFinishPageRoutingModule {}
