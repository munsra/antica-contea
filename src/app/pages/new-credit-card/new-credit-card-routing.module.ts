import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewCreditCardPage } from './new-credit-card.page';

const routes: Routes = [
  {
    path: '',
    component: NewCreditCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewCreditCardPageRoutingModule {}
