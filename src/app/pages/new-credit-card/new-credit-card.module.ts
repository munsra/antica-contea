import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewCreditCardPageRoutingModule } from './new-credit-card-routing.module';

import { NewCreditCardPage } from './new-credit-card.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewCreditCardPageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [NewCreditCardPage]
})
export class NewCreditCardPageModule {}
