import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { CreditCard } from 'src/app/models/card';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { IonicComponentService } from 'src/app/services/ionic-component.service';
import { StripeService } from 'src/app/services/stripe.service';

@Component({
  selector: 'app-new-credit-card',
  templateUrl: './new-credit-card.page.html',
  styleUrls: ['./new-credit-card.page.scss'],
})
export class NewCreditCardPage implements OnInit, OnDestroy {

  card: CreditCard;
  currentUser: User;
  currentUserSubscription: Subscription;
  
  constructor(
    private authService: AuthService,
    private stripeService: StripeService,
    private modalController: ModalController,
    private ionicComponentService: IonicComponentService,
  ) { 

    
  }

  ionViewWillEnter() {

    this.currentUser = this.authService.getCurrentUser();

    this.currentUserSubscription = this.authService.getUserAuthStateChangedvent().subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.card = {
      stripeId: "",
      cardNumber: "",
      expires: null,
      ccv: null,
      brand: null,
      type: null,
      isValid: false
    }
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }

  addCard(){
    this.ionicComponentService.presentTimeoutLoading(3000, true);
    const cardBody = {
      cardNumber: this.card.cardNumber.replace(/\s/g, ""),
      cardExpMonth: new Date(this.card.expires).getMonth() + 1,
      cardExpYear: new Date(this.card.expires).getFullYear(),
      cvc: this.card.ccv
    };
    this.stripeService.createCardToken(cardBody).subscribe((res: any) => {
      if(res.code == 'invalid_number'){
        window.alert('invalid_number');
        return;
      }
      const sourceToken = res.id;
      const addCardToCustomerBody = {
        sourceToken: sourceToken,
        userId: this.currentUser.stripeId
      };
      this.stripeService.addCardToCustomer(addCardToCustomerBody).subscribe((res: any) => {
        if(res.code == 'card_declined'){
          window.alert('card_declined');
          return;
        }
        const cardElement = res;
        this.card.stripeId = cardElement.id;
        this.close();
      })
    })
  }

  async close() {
    await this.modalController.dismiss(this.card);
  }

}
