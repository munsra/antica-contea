import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeerDetailEditPage } from './beer-detail-edit.page';

const routes: Routes = [
  {
    path: '',
    component: BeerDetailEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BeerDetailEditPageRoutingModule {}
