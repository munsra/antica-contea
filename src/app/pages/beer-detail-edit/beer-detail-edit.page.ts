import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { ItemAttributeEditComponent } from 'src/app/components/modals/item-attribute-edit/item-attribute-edit.component';
import { BeerService } from 'src/app/services/beer.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Location } from '@angular/common'
import { IonicComponentService } from 'src/app/services/ionic-component.service';

@Component({
  selector: 'app-beer-detail-edit',
  templateUrl: './beer-detail-edit.page.html',
  styleUrls: ['./beer-detail-edit.page.scss'],
})
export class BeerDetailEditPage implements OnInit {

  parentPath: any;

  //**** toolbar for hide and show ****/
  showToolbar = false;
  showColor = false;
  showTitle = false;
  transition: boolean = false;
  quantity: number = 1;
  item: Observable<any>;
  itemArray: any = [];
  itemOptions: any = [];
  itemSubscribe: any;
  // relatedPlacesArray: any=[];
  // reviews: Observable<any[]>;
  itemId: any;
  categoryId: any;
  showAttribute1: boolean;
  showAttribute2: boolean;

  //**** Size and Addon  ****//
  selectSize: any;

  //**** User authentication  ****//
  userAuth: boolean = false; // Is user logged in ?
  userId: any;
  file: File;
  fileUrl: any;
  hasANewFile: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private beerService: BeerService,
    private modalController: ModalController,
    public router: Router,
    public formBuilder: FormBuilder,
    private ionicComponentService: IonicComponentService,
    private firebaseService: FirebaseService,
    private location: Location
  ) {
    this.itemId = this.activatedRoute.snapshot.paramMap.get('itemId');
  }

  async ngOnInit() {
    this.populateNewItem();
    if (this.itemId != 'new') {
      this.populateItemDetail();
    }
  }

  async populateItemDetail() {
    this.item = await this.beerService.getItemDetail(this.itemId);
    this.itemSubscribe = this.item.subscribe(res => {
      this.itemArray = res;
      this.selectSize = this.itemArray.attribute1_value[0];
    });
  }

  async populateNewItem() {
    this.itemArray = {
      attribute1_value: [],
      attribute1_visible: true,
      categoryId: "",
      description: null,
      image: null,
      name: null,
      rating: 4,
      uid: this.firebaseService.GeNewUid()
    }
  }

  ngOnDestroy() {
    this.unsubscribeToAll();
  }

  unsubscribeToAll(){
    if(this.itemSubscribe) this.itemSubscribe.unsubscribe();
  }

  closeModal() {
    this.modalController.dismiss();
  }

  updateItem() {
    this.ionicComponentService.presentTimeoutLoading(1000, true);
    if (!this.isItemValid()) {
      return;
    }

    if(this.hasANewFile){
      this.setItemWithFile();
    }else{
      this.beerService.updateItem(this.itemArray).then(() => {
        this.location.back();
        this.ionicComponentService.presentToastWithOptions("Notification", "notifications-outline", "", "You've update this item", "top", 5000);
      });
    }
  }

  removeItem() {
    this.ionicComponentService.presentTimeoutLoading(1000, true);
    this.unsubscribeToAll();
    this.beerService.removeItem(this.itemArray).then(() => {
      this.location.back();
      this.ionicComponentService.presentToastWithOptions("Notification", "notifications-outline", "", "You've deleted this item", "top", 5000);
    });
  }

  onScroll($event: CustomEvent) {
    if ($event && $event.detail && $event.detail.scrollTop) {
      const scrollTop = $event.detail.scrollTop;
      this.transition = true;
      this.showToolbar = scrollTop >= 50;
      //console.log("showToolbar="+this.showToolbar);
      this.showTitle = scrollTop >= 50;
      //console.log("showTitle="+this.showTitle);
    } else {
      this.transition = false;
    }
  }

  itemValueChange() {
    if (this.itemArray && this.selectSize) {
      this.itemArray.price = this.selectSize.price * this.quantity;
    }
  }

  async editSize(index, item) {

    const size = this.itemArray.attribute1_value[index];
    const modal = await this.modalController.create({
      component: ItemAttributeEditComponent,
      componentProps: {
        'size': size
      }
    });

    modal.onDidDismiss()
      .then((data) => {
        if (data?.data && data?.data['remove']) {
          this.removeSize(index, item);
        }
      });

    return await modal.present();
  }

  addSize() {
    const newSize = {
      price: 3,
      value: '33cl'
    };
    this.itemArray.attribute1_value.push(newSize);
  }

  removeSize(index, item) {
    this.itemArray.attribute1_value.splice(index, 1);
  }

  isItemValid() {
    let isValid = true;
    if(!this.itemArray.image && !this.fileUrl){
      window.alert("Please choose an image.");
      isValid = false; 
    }
    if (this.itemArray.attribute1_value) {
      if(this.itemArray.attribute1_value.length == 0){
        window.alert("Please insert a size.");
        isValid = false;
      }
      this.itemArray.attribute1_value.forEach(size => {
        if (!size.value || size.value.length <= 0 || size.value == '0cl') {
          window.alert("Size value is not valid.");
          isValid = false;
        }
        if (size.value <= 0) {
          window.alert("Size price is not valid.");
          isValid = false;
        }
      });
    }
    return isValid;
  }

  onFileChange(event) {
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
      this.hasANewFile = true;
    }
  }

  setItemWithFile(){
    this.firebaseService.UploadFile('/products/' + this.itemArray.uid, this.file).subscribe(async newFile => {
      if(newFile){
        await newFile.ref.getDownloadURL().then(url => {
          this.itemArray.image = url;
          this.hasANewFile = false;
          this.beerService.updateItem(this.itemArray).then(() => {
            this.location.back();
            this.ionicComponentService.presentToastWithOptions("Notification", "notifications-outline", "", "You've update this item", "top", 5000);
          });
        }).catch(err => {
          console.log(err);
        });
      }
    });
  }
}
