import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeerDetailEditPageRoutingModule } from './beer-detail-edit-routing.module';

import { BeerDetailEditPage } from './beer-detail-edit.page';
import { ItemAttributeEditComponent } from 'src/app/components/modals/item-attribute-edit/item-attribute-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    BeerDetailEditPageRoutingModule
  ],
  declarations: [BeerDetailEditPage, ItemAttributeEditComponent]
})
export class BeerDetailEditPageModule {}
