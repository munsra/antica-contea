import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Shop } from 'src/app/models/shop';
import { BeerService } from 'src/app/services/beer.service';
import { ShopService } from 'src/app/services/shop.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit, OnDestroy {

  shop: Shop;
  shopSubscription: Subscription;

  cart = [];

  constructor(
    private shopService: ShopService,
    private beerService: BeerService,
  ) { 
    this.cart = this.beerService.getCart();
    this.shopSubscription =  this.shopService.getShop("pdgfiFxqrdNhCzsEb72y").get().subscribe(shop => {
      this.shop = shop.data() as Shop;
    });
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.shopSubscription.unsubscribe();
  }

}
