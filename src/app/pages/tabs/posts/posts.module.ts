import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostsPageRoutingModule } from './posts-routing.module';

import { PostsPage } from './posts.page';
import { AddNewPostComponent } from 'src/app/components/modals/add-new-post/add-new-post.component';
import { PostCardComponent } from 'src/app/components/post-card/post-card.component';
import { PostInputComponent } from 'src/app/components/post-input/post-input.component';
import { PostCardSubitemsComponent } from 'src/app/components/post-card-subitems/post-card-subitems.component';
import { PostCommentCardComponent } from 'src/app/components/modals/post-comment-card/post-comment-card.component';
import { PostCommentItemComponent } from 'src/app/components/post-comment-item/post-comment-item.component';
import { PostCommentInputComponent } from 'src/app/components/post-comment-input/post-comment-input.component';
import { PostCardMenuComponent } from 'src/app/components/modals/post-card-menu/post-card-menu.component';
import { PostCardCommentMenuComponent } from 'src/app/components/modals/post-card-comment-menu/post-card-comment-menu.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PostsPageRoutingModule,
    TranslateModule
  ],
  declarations: [
    PostsPage, 
    AddNewPostComponent, 
    PostCardComponent, 
    PostInputComponent, 
    PostCommentCardComponent, 
    PostCardSubitemsComponent,
    PostCommentItemComponent,
    PostCommentInputComponent,
    PostCardCommentMenuComponent,
    PostCardMenuComponent]
})
export class PostsPageModule {}
