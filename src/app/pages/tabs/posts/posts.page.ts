import { stringify } from '@angular/compiler/src/util';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AddNewPostComponent } from 'src/app/components/modals/add-new-post/add-new-post.component';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { PostService } from 'src/app/services/post.service';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit, OnDestroy {

  posts: Post[] = [];
  cart = [];
  postSubscription: Subscription;

  //**** User authentication  ****/
  userAuth: boolean = false; // Is user logged in ?
  userId: any;
  isUserAdmin: boolean = false;
  currentUser: any;

  getUserSubscription: Subscription;

  constructor(
    private postService: PostService,
    private sharedService: SharedService,
    private authService: AuthService,
    private beerService: BeerService,
    private userService: UserService

  ) {
    
  }


  async ngOnInit() {
    this.postSubscription = this.sharedService.getPostItemLoadEvent().subscribe(res => {
      this.refreshPosts();
    });
  }

  async ionViewWillEnter() {
    this.currentUser = this.authService.getCurrentUser();
    this.cart = this.beerService.getCart();
  }
  

  async ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }

  ionViewDidEnter() {
    this.refreshPosts();
  }

  handleUpdateResponse(post) {

  }

  handleError(err) {

  }

  refreshPosts() {
    this.postService.getPosts().get().subscribe(post => {
      this.posts = [];
      post.forEach((doc) => {
        const post = doc.data();
        this.posts.push(post);
      });
    });
  }

}
