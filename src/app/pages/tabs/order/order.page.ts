import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { OrderDetailModalComponent } from 'src/app/components/modals/order-detail-modal/order-detail-modal.component';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { OrderService } from 'src/app/services/order.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit, OnDestroy {

  orders = [];

  currentUser: User;
  userId: string;
  userAuth: boolean;
  getUserSubscription: Subscription;
  galleryView: string = "orders";

  get MyOrders() {
    return this.orders.filter(x => x.userAdminUid == this.currentUser.uid);
  }

  constructor(
    private beerService: BeerService,
    private orderService: OrderService,
    private authService: AuthService,
    private userService: UserService,
    private modalController: ModalController
  ) {

  }
  ngOnDestroy(): void {
    this.getUserSubscription.unsubscribe();
  }

  async ngOnInit() {

  }


  async ionViewWillEnter() {
    this.orderService.getBeerOrders().subscribe(orders => {
      this.orders = orders;
    });

    //this.userAuth =  await this.userService.getAuthState();
    this.userAuth = await this.authService.isLoggedIn();
    this.userId = await this.authService.getUserId();
    if (this.userId && this.userId !== "") {
      this.getUserSubscription = this.userService.getUser(this.userId).get().subscribe(user => {
        this.currentUser = user.data();
      })
    }
  }

  openDetailOrder(order) {
    console.log(order);
    this.modalController.create({
      component: OrderDetailModalComponent,
      componentProps: {
        data: order,
        // index: image
      }
    }).then(modal => {
      modal.present();
    });
  }

  takeOrder(order) {
    this.orderService.takeOrder(this.currentUser, order);
  }

  completedOrder(order){
    this.orderService.completeOrder(this.currentUser, order);
  }

}
