import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { BeerDetailEditPage } from '../../beer-detail-edit/beer-detail-edit.page';
import { BeerDetailPage } from '../../beer-detail/beer-detail.page';
import { faWineGlass } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-beers',
  templateUrl: './beers.page.html',
  styleUrls: ['./beers.page.scss'],
})
export class BeersPage implements OnInit, OnDestroy {
  faWineGlass = faWineGlass;
  public items: Observable<any[]>;
  cart = [];
  currentUser: User;
  currentUserSubscription: Subscription;

  constructor(
    private beerService: BeerService,
    private modalController: ModalController,
    private authService: AuthService,
    private router: Router,
  ) {
    this.currentUserSubscription = this.authService.getUserAuthStateChangedvent().subscribe(user => {
      this.currentUser = user;
    });
  }

  async ngOnInit() {
    this.items = this.beerService.getItems();

    this.currentUser = this.authService.getCurrentUser();
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }

  ionViewWillEnter(){
    this.cart = this.beerService.getCart();
  }

  addNewItem(){

  }

  goToDetail(itemId: string){
    if(this.currentUser && this.currentUser.isAdmin){
      this.router.navigate(['/beer-detail-edit', itemId]);
    }else{
      this.router.navigate(['/beer-detail', itemId]);
    }
  }

}
