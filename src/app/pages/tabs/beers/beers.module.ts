import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeersPageRoutingModule } from './beers-routing.module';

import { BeersPage } from './beers.page';
import { TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BeersPageRoutingModule,
    TranslateModule,
    FontAwesomeModule
  ],
  declarations: [BeersPage]
})
export class BeersPageModule {}
