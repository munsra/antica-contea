import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  currentUser: User;
  file: File;
  darkSide = true;

  constructor(
    private sharedService: SharedService,
    private authService: AuthService,
    private userService: UserService) { }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
  }

  toggleDarkMode(){
    this.sharedService.sendToogleDarkTheme(this.currentUser.userSettings.darkModeActive);
  }

  uploadImage(event){
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.currentUser.photoURL = event.target.result.toString();
    }
  }

  saveChanges(){
    if(this.file){
      this.userService.uploadUserImage(this.file).subscribe(file => {
        if (file) {
          file.ref.getDownloadURL().then(url => {
            this.currentUser.photoURL = url;
            this.authService.setUserData(this.currentUser);
          }).catch(err => {
            console.log(err);
          });
        }
      });
    }
    else{
      this.authService.setUserData(this.currentUser);
    }
    
  }

}
