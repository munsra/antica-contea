import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Banner } from 'src/app/models/banners';
import { Shop } from 'src/app/models/shop';
import { BeerService } from 'src/app/services/beer.service';
import { ShopService } from 'src/app/services/shop.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  shop: Shop;
  banners: Banner[] = [];
  cart = [];
  shopSubscription: Subscription;
  bannersSubscription: Subscription;
  
  constructor(
    private shopService: ShopService,
    private beerService: BeerService
  ) { 
    
    this.shopSubscription =  this.shopService.getShop("pdgfiFxqrdNhCzsEb72y").get().subscribe(shop => {
      this.shop = shop.data() as Shop;
    });

    this.bannersSubscription = this.shopService.getBanners().subscribe(banners => {
      this.banners = banners.filter(x => x.enable);
    })
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.cart = this.beerService.getCart();
  }

  ngOnDestroy(): void {
    this.shopSubscription.unsubscribe();
    this.bannersSubscription.unsubscribe();
  }

}
