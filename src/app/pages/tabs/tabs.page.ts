import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  currentUser: any;
  currentUserSubscription: Subscription;

  constructor(
    private authService: AuthService
  ) { 
    this.currentUserSubscription = this.authService.getUserAuthStateChangedvent().subscribe(user => {
      this.currentUser = user;
      if(this.currentUser) this.authService.observableCurrentUser()
    });
  }

  async ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
  }

}
