import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreditCard } from 'src/app/models/card';
import { getValidationConfigFromCardNo } from './helpers/card.helper';
import { luhnCheck } from './helpers/luhn.helper';
import { PickerController } from '@ionic/angular';
import { PickerOptions } from "@ionic/core";

@Component({
  selector: 'app-checkout-credit-card',
  templateUrl: './checkout-credit-card.page.html',
  styleUrls: ['./checkout-credit-card.page.scss'],
})
export class CheckoutCreditCardPage implements OnInit {
  @Input() card: CreditCard;

  months: string[] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

  public creditCardForm: FormGroup;
  cardLogoUrl = "";
  isCardNumberCorrect: boolean = true;
  errorMessage = "";
  constructor(
    public formBuilder: FormBuilder,
    private pickerController: PickerController
  ) {
    this.cardLogoUrl = "assets/images/chip.png";
  }

  ngOnInit() {
  }

  getCardNumberControl(): AbstractControl | null {
    return this.creditCardForm && this.creditCardForm.get('cardNumber');
  }

  cardValueChange(event) {
    this.card.cardNumber = event;
    this.checkCardLogo(event);
    this.checkCardValidation();
  }

  expMonthChange(event) {
    this.card.expires = event;
    this.checkCardValidation();
  }

  expYearChange(event) {
    this.card.expires = event;
    this.checkCardValidation();
  }

  ccvValueChange(event) {
    if (event > 9999) {
      this.card.ccv = +this.card.ccv.toString().slice(0, -1);
    }else{
      this.card.ccv = event;
    }
    this.checkCardValidation();
  }

  cardMaskFunction(rawValue: string): Array<RegExp> {
    const card = getValidationConfigFromCardNo(rawValue);
    if (card) {
      return card.mask;
    }
    return [/\d/];
  }

  checkCardLogo(cardNumber) {
    const card = getValidationConfigFromCardNo(cardNumber);
    if (card) {
      switch (card.type) {
        case 'VISA':
          this.card.type = "visa";
          this.cardLogoUrl = 'assets/images/visa.png';
          break;
        case 'AMERICANEXPRESS':
          this.card.type = "AMERICANEXPRESS";
          this.cardLogoUrl = 'assets/images/american.png';
          break;
        case 'MASTERCARD':
          this.card.type = "MASTERCARD";
          this.cardLogoUrl = 'assets/images/mastercard.png';
          break;
        default:
          this.card.type = "NONE";
          this.cardLogoUrl = 'assets/images/chip.png';
          break;
      }
    } else {
      this.cardLogoUrl = 'assets/images/chip.png';
    }
    /*
      VISA = 'VISA',
      MASTERCARD = 'MASTERCARD',
      AMERICANEXPRESS = 'AMERICANEXPRESS',
      DISCOVER = 'DISCOVER',
      DINERSCLUB = 'DINERSCLUB',
      JCB = 'JCB',
      MAESTRO = 'MAESTRO',
      UNIONPAY = 'UNIONPAY',
      DANKORT = 'DANKORT',
      FORBRUGSFORENINGEN = 'FORBRUGSFORENINGEN'
    */
  }

  checkCardValidation() {

    if (!this.card.cardNumber || !luhnCheck(this.card.cardNumber)) {
      this.isCardNumberCorrect = false;
      this.card.isValid = false;
      this.card.errorMessage = "Check Card Number";
      return
    }
    const cardValuated = getValidationConfigFromCardNo(this.card.cardNumber);

    if (!this.card.ccv) {
      this.isCardNumberCorrect = false;
      this.card.isValid = false;
      this.card.errorMessage = "Check CCV";
      return;
    } else {
      if (this.card.ccv.toString().length < Math.min(...cardValuated.cvvLength) || 
          this.card.ccv.toString().length > Math.max(...cardValuated.cvvLength)) {
        this.isCardNumberCorrect = false;
        this.card.isValid = false;
        this.card.errorMessage = "Check CCV";
        return;
      }
    }
    if (!this.card.expires) {
      this.isCardNumberCorrect = false;
      this.card.isValid = false;
      this.card.errorMessage = "Check Exp. Month";
      return;
    }
    if (!this.card.expires) {
      this.isCardNumberCorrect = false;
      this.card.isValid = false;
      this.card.errorMessage = "Check Exp. Year";
      return;
    }

    this.isCardNumberCorrect = true;
    this.card.isValid = true;
  }

  async showMonthPicker() {
    let options: PickerOptions = {
      buttons: [
        {
          text: "Cancel",
          role: 'cancel'
        },
        {
          text:'Ok',
          handler:(value:any) => {
            console.log(value);
          }
        }
      ],
      columns:[{
        name:'Month',
        options:this.getMonthColumnOptions()
      }]
    };

    let picker = await this.pickerController.create(options);
    picker.present()
  }

  getMonthColumnOptions(){
    let options = [];
    this.months.forEach(x => {
      options.push({text:x,value:x});
    });
    return options;
  }

}
