import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckoutCreditCardPageRoutingModule } from './checkout-credit-card-routing.module';

import { CheckoutCreditCardPage } from './checkout-credit-card.page';
import { TextMaskModule } from 'angular2-text-mask';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    CheckoutCreditCardPageRoutingModule,
    TextMaskModule,
    TranslateModule
  ],
  exports: [CheckoutCreditCardPage],
  declarations: [CheckoutCreditCardPage]
})
export class CheckoutCreditCardPageModule {}
