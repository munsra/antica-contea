import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { CreditCard } from 'src/app/models/card';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { IonicComponentService } from 'src/app/services/ionic-component.service';
import { OrderService } from 'src/app/services/order.service';
import { ShippingService } from 'src/app/services/shipping.service';
import { StripeService } from 'src/app/services/stripe.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';
import { AddressAddPage } from '../address-add/address-add.page';
import { AddressEditPage } from '../address-edit/address-edit.page';
import { NewCreditCardPage } from '../new-credit-card/new-credit-card.page';

@Component({
  selector: 'app-cart-checkout',
  templateUrl: './cart-checkout.page.html',
  styleUrls: ['./cart-checkout.page.scss'],
})
export class CartCheckoutPage implements OnInit, OnDestroy {
  @ViewChild('content') private content: any;
  //**** User authentication  ****/
  userAuth: boolean = false; // Is user logged in ?
  userId: string;
  currentUser: User;
  cards: CreditCard[] = [];
  //**** Address ****/
  public addresses = [];
  public selectAddressId: string;
  addressesSubscription: Subscription;
  checkedAddress: boolean = false;
  checkedPayment: boolean = false;
  places = [];
  placesSubscription: Subscription;
  isPaymentTypeCreditCard: boolean = false;
  isCardNumberCorrect: boolean = false;
  isAddressBuyOnSite: boolean = false;

  // Stripe URL
  stripeUrl = environment.stripeServerUrl;

  paymentType: string;

  //**** Order ****/
  orderItems = [];
  total = 0;

  //**** Vat,Tax ****/
  vat: number = 0;
  vatTotal: number;

  //**** Delivery fee ****/
  fee: number = 25;
  feeTotal: number;
  valueTotal: number = 0;
  subTotal: any;
  card: CreditCard;

  get grandTotal(): any {
    this.total = this.valueTotal;
    this.subTotal = Number(this.total.toFixed(2));
    this.vatTotal = Number(
      (this.subTotal * this.vat / 100).toFixed(2)
    );
    this.feeTotal = Number((this.fee + this.vatTotal).toFixed(2));

    return Number((this.subTotal + this.feeTotal).toFixed(2));;
  }

  get currentAddress(): any {
    return this.addresses.filter(x => x.id == this.selectAddressId)[0];
  }

  public item: Observable<any[]>;
  private userProfileId: any;
  private currentUserSubscription: Subscription;
  private stripeRetriveUserSubscription: Subscription;
  private stripeCreditCardSubscription: Subscription;

  itemCart: any;
  orderId: any;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    public beerService: BeerService,
    private orderService: OrderService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    public modalController: ModalController,
    public router: Router,
    private fbService: FirebaseService,
    private shippingService: ShippingService,
    private stripeService: StripeService,
    private ionicComponentService: IonicComponentService,
    private http: HttpClient
  ) {
    
    
  }

  async ngOnInit() {
    this.card = {
      stripeId: null,
      cardNumber: "",
      expires: null,
      ccv: null,
      brand: null,
      type: null,
      isValid: false
    }
  }

  ngOnDestroy(): void {
    if(this.currentUserSubscription) this.currentUserSubscription.unsubscribe();
    this.placesSubscription.unsubscribe();
    this.addressesSubscription.unsubscribe();
  }

  async ionViewWillEnter() {

    console.log(this.router.url, "Current URL");

    this.currentUser = this.authService.getCurrentUser();

    this.userId = this.authService.getUserId();

    if(this.currentUser){
      this.getStripeCreditCardForCurrentUser();
    }
    
    this.currentUserSubscription = this.authService.getUserAuthStateChangedvent().subscribe(user => {
      this.currentUser = user;
      this.getStripeCreditCardForCurrentUser();
    });

    this.placesSubscription = this.shippingService.getPlaces().subscribe(places => {
      this.places = places;
    });

    this.addressesSubscription = this.userService.getAddressByUserId(this.userId).subscribe(addresses => {
      this.addresses = addresses;
    });

    this.userAuth = await this.authService.isLoggedIn();
    this.getCartProduct()
  }

  

  async openAddressModal() {
    console.log("openModal");
    const modal = await this.modalController.create({
      component: AddressAddPage,
      componentProps: {
      }
    });
    return await modal.present();
  }

  async editAddressModal(addressId: string) {
    console.log("openModal");
    const modal = await this.modalController.create({
      component: AddressEditPage,
      componentProps: {
        addressId: addressId
      }
    });
    return await modal.present();
  }

  addressBuyOnSite(){
    this.fee = 0;
    this.scrollToBottomOnInit();
  }

  addressGroupChange(event) {
    console.log("addressGroupChange", event.detail);
    //this.selectedRadioGroup = event.detail;
    this.selectAddressId = event.detail.value;
    this.checkedAddress = true;
    console.log("selectAddressId=" + this.selectAddressId);
    var currentAddress = this.addresses.filter(x => x.id == this.selectAddressId)[0];
    this.fee = this.places.filter(x => x.id == currentAddress.place.id)[0].price;
    this.scrollToBottomOnInit();
  }

  paymentGroupChange(event) {
    console.log("paymentGroupChange", event.detail);
    //this.selectedRadioGroup = event.detail;
    this.paymentType = event.detail.value;
    this.checkedPayment = true;
    console.log("checkedPayment = " + this.checkedPayment);
    console.log("paymentType=" + this.paymentType);
    if (this.paymentType == "cc") {
      this.isPaymentTypeCreditCard = true;
      this.scrollToBottomOnInit();
    }
    else if(this.paymentType == "cash") {
      this.isPaymentTypeCreditCard = false;
      this.scrollToBottomOnInit();
    }
  }

  //**** Items in cart ****/

  getCartProduct() {
    // this.cartProducts = this.productService.getLocalCartProducts();
    const items = this.beerService.getCart();
    console.log("page not refresh__________________food-cart : items=" + items);
    const selected = {};
    this.valueTotal = 0;
    for (const obj of items) {
      this.valueTotal += obj.price;
    }

    // all item in shopping cart
    this.orderItems = items;
    console.log("..............Order items =" + JSON.stringify(this.orderItems));
    this.total = this.valueTotal;
    this.subTotal = Number(this.total.toFixed(2));
    this.vatTotal = Number(
      (this.subTotal * this.vat / 100).toFixed(2)
    );
    this.feeTotal = Number((this.fee + this.vatTotal).toFixed(2));
  }
  //**** Place order ****/

  placeOrder() {
    if (this.checkedAddress) {
      console.log("selectAddressId = " + this.selectAddressId);
      if (this.checkedPayment) {
        if (this.paymentType == "cc") {
          if(this.card.stripeId){
            this.stripeOrder();
          }
        }
        else if (this.paymentType == "cash") {
          this.addOrder();
        }
      } else {
        this.ionicComponentService.presentAlert("Please choose payment method");
      }
    } else {
      this.ionicComponentService.presentAlert("Please choose address");
    }
  }

  creditCardSelection(event){
    this.card = this.cards.filter(x => x.stripeId == event.detail.value)[0];
    console.log("selected card");
    console.log(this.card);
  }

  async openCreditCardModal(){
    console.log("openModal");
    const modal = await this.modalController.create({
      component: NewCreditCardPage
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.card = dataReturned.data;
        if(this.card.stripeId){
          this.getStripeCreditCardForCurrentUser();
        }
      }
    })

    return await modal.present();
  }

  async stripeOrder() {
    this.ionicComponentService.presentLoading();
    const paymentIntentBody = {
      amount: this.grandTotal * 100,
      currency: "eur",
      custId: this.currentUser.stripeId,
      shipping: {
        name: this.currentAddress.fullname,
        address: {
          line1: this.currentAddress.address1,
          city: this.currentAddress.city,
          postal_code: this.currentAddress.cap,
          state: "IT",
          country: "IT"
        }
      }
    }
    this.stripeService.createPaymentIntent(paymentIntentBody).subscribe((res: any) => {
      if(res.code == 'amount_too_small'){
        window.alert('amount_too_small');
        this.ionicComponentService.dismissLoading();
        return;
      }
      const paymentIntentToken = res.cus.id;
      const confirmPIBody = {
        paymentIntendId: paymentIntentToken,
        payment_method: this.card.stripeId
      }
      this.stripeService.confirmPaymentIntent(confirmPIBody).subscribe((res: any) => {
        const newOrder = {
          userProfileId: this.userId,
          userProfile: {
            userProfileId: this.userId,
            userFirstname: this.currentUser.firstname,
            userLastname: this.currentUser.lastname,
          },
          addressId: this.selectAddressId,
          address: this.currentAddress,
          orderId: this.fbService.GeNewUid(),
          paymentType: this.paymentType,
          orderItems: this.orderItems,
          status: "pending",
          totalPrice: this.grandTotal,
          createdTime: new Date(),
          shipmentFee: this.fee,
          stripePaymentIntent: paymentIntentToken
        }
        this.orderService.placeOrder(newOrder).then(
          () => {
            //this.router.navigateByUrl('/home');
            console.log("New item added.")
            //this.ionicComponentService.dismissLoading();
            this.router.navigateByUrl('/cart-checkout-finish');
            //this.navController.goForward(`/person/${this.catId}?something=${encodeURI(somethingValue)}`);
            //this.navController.goForward('/crud-item/${this.catId}');
            //this.navController.navigateForward('/crud-item');
          },
          error => {
            console.log(error);
            //this.ionicComponentService.presentToast(error, 3000);
            //this.ionicComponentService.dismissLoading();
          }
        );
      })
    })

    console.log(this.currentUser.stripeId);
  }



  async addOrder() {
    // console.log("userProfileId="+this.userService.getUserId());
    console.log("_____call addAddress");
    this.ionicComponentService.presentLoading();
    const newOrder = {
      userProfileId: this.userId,
      userProfile: {
        userProfileId: this.userId,
        userFirstname: this.currentUser.firstname,
        userLastname: this.currentUser.lastname,
      },
      addressId: this.selectAddressId,
      address: this.currentAddress,
      orderId: this.fbService.GeNewUid(),
      paymentType: this.paymentType,
      orderItems: this.orderItems,
      status: "pending",
      totalPrice: this.grandTotal,
      createdTime: new Date(),
      shipmentFee: this.fee
    }
    this.orderService.placeOrder(newOrder)
      .then(
        () => {
          //this.router.navigateByUrl('/home');
          console.log("New item added.")
          this.ionicComponentService.dismissLoading();
          this.router.navigateByUrl('/cart-checkout-finish');
          //this.navController.goForward(`/person/${this.catId}?something=${encodeURI(somethingValue)}`);
          //this.navController.goForward('/crud-item/${this.catId}');
          //this.navController.navigateForward('/crud-item');
        },
        error => {
          console.log(error);
          this.ionicComponentService.presentToast(error, 3000);
          this.ionicComponentService.dismissLoading();
        }
      );
  }

  addNewStripeUser(){
    const addCustomerBody = {
      cusName: this.currentUser.displayName,
      cusEmail: this.currentUser.email
    }

    this.stripeService.addCustomer(addCustomerBody).subscribe((res : any) => {
      this.currentUser.stripeId = res.cus.id;
      this.authService.setUserData(this.currentUser);
    });
  }


  scrollToBottomOnInit() {

    setTimeout(() => {                           //<<<---using ()=> syntax
      this.content.scrollToBottom(300);
    }, 300);
  }

  getStripeCreditCardForCurrentUser(){

    var getAllCardsBoody = {
      custId: this.currentUser.stripeId
    };

    this.stripeCreditCardSubscription = this.stripeService.getAllCard(getAllCardsBoody).subscribe((cards: any) => {
      this.cards = [];
      console.log(cards.data);
      if(cards.data){
        cards.data.forEach((card) => {
          // doc.data() is never undefined for query doc snapshots
          const itemCard: CreditCard = {
            stripeId: card.id,
            cardNumber: "**** **** **** " + card.last4,
            expires: card.exp_month + card.exp_year,
            ccv: null,
            brand: card.brand,
            type: null,
          };
          this.cards.push(itemCard);
        });
      }
      console.log(this.cards);
    });
  }

  removeCartProduct(index, slidingItem) {
    //this.productService.removeLocalCartProduct(product);
    slidingItem.close();
    console.log("removeCart")
    this.beerService.removeItemCart(index);
    // Recalling
    this.getCartProduct();
  }
}
