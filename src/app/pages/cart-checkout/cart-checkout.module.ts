import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartCheckoutPageRoutingModule } from './cart-checkout-routing.module';

import { CartCheckoutPage } from './cart-checkout.page';
import { TranslateModule } from '@ngx-translate/core';
import { CheckoutCreditCardPageModule } from '../checkout-credit-card/checkout-credit-card.module';
import { CheckoutCreditCardPage } from '../checkout-credit-card/checkout-credit-card.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    CartCheckoutPageRoutingModule,
    TranslateModule,
    CheckoutCreditCardPageModule
  ],
  declarations: [CartCheckoutPage],
  entryComponents: [CheckoutCreditCardPage]
})
export class CartCheckoutPageModule {}
