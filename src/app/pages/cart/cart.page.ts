import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  selectedItems = [];
  total = 0;
  totalVat: number;
  totalValue: number = 0;
  //**** User authentication  ****/
  userAuth: boolean = false; // Is user logged in ?
  userId: any;

  constructor(
    private beerService: BeerService,
    private authService: AuthService,
    public alertController: AlertController,
    private router: Router
  ) { }

  async ngOnInit() {
   
  }
  async ionViewWillEnter() {
    //this.userAuth =  await this.userService.getAuthState();
    this.userAuth =  await this.authService.isLoggedIn();
    this.userId = await this.authService.getUserId();
    this.getCartProduct() 
    console.log("___this.userAuth="+this.userAuth);
    console.log("___this.userId="+this.userId);
   // this.totalVat = this.total + ]
  }

  removeCartProduct(index, slidingItem) {
    //this.productService.removeLocalCartProduct(product);
    slidingItem.close();
    console.log("removeCart")
    this.beerService.removeItemCart(index);
    // Recalling
    this.getCartProduct();
  }

  getCartProduct() {
    // this.cartProducts = this.productService.getLocalCartProducts();
    const items = this.beerService.getCart();
    console.log("____food-cart : items=" + items);
    const selected = {};

    this.totalValue = 0;
    for (const obj of items) {
      this.totalValue += obj.price;
    }

    this.selectedItems = items;
    console.log("selected Addon=" + this.selectedItems);
    this.total = this.totalValue;

  }

  // checkout
  checkout() {
    //navigate to address page
    // check if user already logged in?
    if (!this.userAuth) {
      // error: not login 
      // show popup
      console.log("checkout()___userAuth = false");
      this.presentAlertConfirm();
    } else {
      // userAuth = true 
      console.log("checkout()___userAuth = true");
      this.router.navigateByUrl('/cart-checkout');
    }
    //
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Attenzione',
      message: 'Devi autenticarti prima!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Login',
          handler: () => {
            console.log('Go to login page');
            
            this.router.navigateByUrl('/fire-signin?redirectUrl=cart-checkout');
          }
        }
      ]
    });
  
    await alert.present();
  }
  

}
