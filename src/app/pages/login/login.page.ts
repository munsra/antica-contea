import { Component, OnInit, ɵConsole } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User, UserSettings } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentialForm: FormGroup = new FormGroup({
    email: new FormControl('munsra3@gmail.com', [Validators.required, Validators.pattern("^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")]),
    password: new FormControl('123456', [Validators.required, Validators.minLength(6)]),
  });

  isSubmitted = false;
  constructor(private firestore: AngularFirestore,
    private authService : AuthService,
    private ngFireAuth: AngularFireAuth,
    private userService: UserService,
    private firebaseService: FirebaseService,
    private chatService: ChatService,
    private router: Router,
    private sharedService: SharedService) {
     }

  ngOnInit() {
    this.checkIfUserIsLoggedInAndExists();
  }

  checkIfUserIsLoggedInAndExists(){
    if(this.authService.isLoggedIn){
      const jsonUser = localStorage.getItem('user');
      if(jsonUser){
        const user = JSON.parse(localStorage.getItem('user')) as User;
        this.authService.getUser(user.uid).subscribe(userStored => {
          if(!userStored.exists){
            window.alert('Please sign up. User not found.');
          }else{
            const currentUserItem = userStored.data() as User;
            this.userService.setCurrentUser(currentUserItem);
            //this.router.navigate(['tabs']);
          }
        })
      }
    }
  }

  get errorControl() {
    return this.credentialForm.controls;
  }

  async logIn() {
    this.isSubmitted = true;
    if (!this.credentialForm.valid) {
      return false;
    }
    const email = this.credentialForm.get('email').value;;
    const password = this.credentialForm.get('password').value;
    await this.sharedService.presentLoading('Login User ...');
   
    this.authService.signIn(email, password)
    .then(res => {
      if(res.user.emailVerified) {
        let newUser = User.getUserFromFirebaseUser(res.user);
        // Check if user exist
        this.authService.getUser(res.user.uid).subscribe(userStored => {
          
          if(!userStored.exists){
            newUser.userSettings = User.setUserSettings(newUser.uid, false);
            this.authService.setUserData(newUser);
          }else{
            newUser = userStored.data() as User;
          }
          this.userService.setCurrentUser(newUser);
          this.sharedService.sendToogleDarkTheme(newUser.userSettings.darkModeActive)
          this.router.navigate(['tabs']);
        })
      } else {
        window.alert('Email is not verified');
        return false;
      }
    }).catch(err =>{
      switch(err.code){
        case 'auth/user-not-found':
        window.alert('Please sign up. User not found.');
      }
      console.log(err);
    }).finally(async () =>{
      await this.sharedService.dismissLoading();
    });
  }

  async loginGuest(){
    await this.sharedService.presentLoading('Login User ...');
    this.authService.signInAnonymously()
    .then(res => {
      if(res.user.isAnonymous) {
        // Check if user exist
        this.authService.getUser(res.user.uid).subscribe(userStored => {
          const newUser = User.getUserFromFirebaseUser(res.user);
          if(!userStored.exists){
            newUser.userSettings = User.setUserSettings(newUser.uid, false);
            this.authService.setUserData(newUser);
          }
          localStorage.setItem('user',JSON.stringify(newUser) )
          this.router.navigate(['tabs']);
        })
      } else {
        window.alert('Email is not verified');
        return false;
      }
    }).catch(err =>{
      switch(err.code){
        case 'auth/user-not-found':
        window.alert('Please sign up. User not found.');
      }
      console.log(err);
    }).finally(async () =>{
      await this.sharedService.dismissLoading();
    });
  }

  onSignup() {
    this.router.navigate(['registration']);    
  }

  GoogleAuth(){
    this.sharedService.presentLoading('Login User ...');
    this.authService.googleAuth().finally(() => {
      this.sharedService.dismissLoading();
    });
  }

}
