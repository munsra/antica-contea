import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatPageRoutingModule } from './chat-routing.module';

import { ChatPage } from './chat.page';
import { ChatBubbleComponent } from 'src/app/components/chat-bubble/chat-bubble.component';
import { ChatInputComponent } from 'src/app/components/chat-input/chat-input.component';
import { ChatSendImageComponent } from 'src/app/components/modals/chat-send-image/chat-send-image.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatPageRoutingModule
  ],
  declarations: [ChatPage, ChatBubbleComponent, ChatInputComponent, ChatSendImageComponent]
})
export class ChatPageModule {}
