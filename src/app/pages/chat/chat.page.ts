import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent, IonList } from '@ionic/angular';
import { Chat } from 'src/app/models/chat';
import { User } from 'src/app/models/user';
import { Message } from 'src/app/models/message';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';
import { SharedService } from 'src/app/services/shared.service';
import { NgZone } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, OnDestroy {
  chat: Chat;
  currentUser: User;
  messages: Message[];
  isLoading: boolean = false;
  imagesLoadSubscription: Subscription;
  chatSubscription: Subscription;
  messageSubscription: Subscription;
  @ViewChild(IonContent) content: IonContent;
  @ViewChild(IonList, { read: ElementRef }) list: ElementRef;

  constructor(
    private authService: AuthService,
    private chatService: ChatService,
    private route: ActivatedRoute,
    private router: Router,
    public _zone: NgZone,
    private sharedService: SharedService) {
    this.imagesLoadSubscription = this.sharedService.getChatItemImageLoadEvent().subscribe(() => {
      this.scrollToBottom();
    })
  }
  ngOnDestroy(): void {
    this.chatSubscription.unsubscribe();
    this.messageSubscription.unsubscribe();
    this.imagesLoadSubscription.unsubscribe();
  }

  ngOnInit() {
    this.readCurrentUser();
    this.readChat();
  }

  readCurrentUser() {
    this.currentUser = this.authService.getCurrentUser();
  }

  readChat() {
    this.route.queryParams.subscribe(params => {
    this.chatSubscription = this.chatService.getChat(params.chatUid).subscribe(chat => {
        this.chat = chat.data();
        if (this.chat.type == 1) {
          this.chat.title = params.chatName;
        }
        this.readMessase(this.chat.uid);
      });
    });
  }

  readMessase(chatUid) {
    this.isLoading = true;
    this.messageSubscription = this.chatService.getMessagesFromChatId(chatUid).subscribe(messages => {
      this.isLoading = false;
      this.messages = messages;
      if(!this.messages.find(x => x.mediaUrl != null) != null){
        this.scrollToBottom();
      }
    });
  }

  scrollToBottom() {
    this._zone.run(() => {
      setTimeout(() => {
        this.content.scrollToBottom(300);
      }, 400);
    });
  }

  back() {
    this.router.navigate(['tabs']);
  }

}
