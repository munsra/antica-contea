import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { NavController, ModalController, LoadingController } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { IonicComponentService } from 'src/app/services/ionic-component.service';
import { ShippingService } from 'src/app/services/shipping.service';
import { UserService } from 'src/app/services/user.service';

declare var google;

@Component({
  selector: 'app-address-add',
  templateUrl: './address-add.page.html',
  styleUrls: ['./address-add.page.scss'],
})
export class AddressAddPage implements OnInit, OnDestroy {


  map: any;
  lat: string;
  long: string;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  GoogleAutocomplete: any;
  address: Object;
  addressHasNumber: boolean = true;
  formattedAddress: string;
  places = [];

  placesSubscription: Subscription;

  public addressForm: FormGroup;
  private userProfileId: any;
  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    public userService: UserService,
    public beerService: BeerService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private modalController: ModalController,
    public router: Router,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public zone: NgZone,
    private loadingController: LoadingController,
    public formBuilder: FormBuilder,
    private shippingService: ShippingService,
    private ionicComponentService: IonicComponentService
  ) {
    //this.categoryId = this.activatedRoute.snapshot.paramMap.get('categoryId');
    console.log("Get activatedRoute categoryId=" + this.activatedRoute.snapshot.paramMap.get('categoryId'));
    console.log(this.router.url, "Current URL");
    this.addressForm = this.formBuilder.group({
      label: ['', Validators.required],
      fullname: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(6)]],
      address1: ['', Validators.required],
      address2: [''],
      city: ['', Validators.required],
      place: ['', Validators.required],
      cap: ['', Validators.required]
    });
    this.placesSubscription = this.shippingService.getPlaces().subscribe(places => {
      this.places = places.sort((a,b) => a.sigla.localeCompare(b.sigla));
    });
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
  }

  ngOnInit() {
    this.userProfileId = this.authService.getUserId();
  }

  ngOnDestroy(): void {
    this.placesSubscription.unsubscribe();
  }

  async close() {
    await this.modalController.dismiss();
  }

  submitFormTest() {
    if (!this.addressForm.valid) {
      console.log(this.addressForm.value);
      //this.presentAlert("invalid form");
      console.log("invalid form")
    } else {
      console.log(this.addressForm.value);
      console.log("yes, ")
      //this.userService.loginUser()
    }
  }

  async addAddress() {
    // console.log("userProfileId="+this.userService.getUserId());
    console.log("_____call addAddress");
    if (!this.addressForm.valid) {
      console.log(this.addressForm.value);
      console.log("____addressForm invalid ")
      //this.presentAlert("invalid form");
    } else {

      console.log("ADDRESS");
      console.log(this.addressForm.value.address);
      //****** loading *******//
      const loading = await this.loadingController.create({
        //spinner: null,
        duration: 2000,
        message: '',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });

      //return await loading.present();
      await loading.present();

      const newAddress = {
        uid: this.firebaseService.GeNewUid(),
        userProfileId: this.userProfileId,
        label: this.addressForm.value.label,
        fullname: this.addressForm.value.fullname,
        phone: this.addressForm.value.phone,
        address1: this.addressForm.value.address1,
        address2: this.addressForm.value.address2,
        city: this.addressForm.value.city,
        cap: this.addressForm.value.cap,
        place: this.addressForm.value.place
      };

      //****** add review *******//
      await this.userService.setAddress(newAddress)
        // await console.log("2");
        .then(() => {
          // call loading 
          // close loading
          this.close();
          //this.router.navigateByUrl('/food-checkout');
          //loadingPopup.dismiss();
          //this.nav.setRoot('AfterLoginPage');
        }, (error) => {
          var errorMessage: string = error.message;
          console.log("ERROR:" + errorMessage);
          this.close();
          //loadingPopup.dismiss();
          //this.presentAlert(errorMessage);      
        });

    }
  }

  //AUTOCOMPLETE, SIMPLY LOAD THE PLACE USING GOOGLE PREDICTIONS AND RETURNING THE ARRAY.
  UpdateSearchResults() {
    const address = this.addressForm.get('address').value;
    if (address == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: address },
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

  ClearAutocomplete() {
    this.autocompleteItems = []
    this.addressForm.get('address').setValue('');
  }

  getAddress(place: object) {
    this.address = place['formatted_address'];
    this.addressForm.controls["phone"].setValue(this.getPhone(place));
    this.formattedAddress = place['formatted_address'];
    this.zone.run(() => this.formattedAddress = place['formatted_address']);
  }

  getAddrComponent(place, componentTemplate) {
    let result;

    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      if (componentTemplate[addressType]) {
        result = place.address_components[i][componentTemplate[addressType]];
        return result;
      }
    }
    return;
  }

  getStreetNumber(place) {
    const COMPONENT_TEMPLATE = { street_number: 'short_name' },
      streetNumber = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return streetNumber;
  }

  getStreet(place) {
    const COMPONENT_TEMPLATE = { route: 'long_name' },
      street = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return street;
  }

  getCity(place) {
    const COMPONENT_TEMPLATE = { locality: 'long_name' },
      city = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return city;
  }

  getState(place) {
    const COMPONENT_TEMPLATE = { administrative_area_level_1: 'short_name' },
      state = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return state;
  }

  getDistrict(place) {
    const COMPONENT_TEMPLATE = { administrative_area_level_2: 'short_name' },
      state = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return state;
  }

  getCountryShort(place) {
    const COMPONENT_TEMPLATE = { country: 'short_name' },
      countryShort = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return countryShort;
  }

  getCountry(place) {
    const COMPONENT_TEMPLATE = { country: 'long_name' },
      country = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return country;
  }

  getPostCode(place) {
    const COMPONENT_TEMPLATE = { postal_code: 'long_name' },
      postCode = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return postCode;
  }

  getPhone(place) {
    const COMPONENT_TEMPLATE = { formatted_phone_number: 'formatted_phone_number' },
      phone = this.getAddrComponent(place, COMPONENT_TEMPLATE);
    return phone;
  }
}
