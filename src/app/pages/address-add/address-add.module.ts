import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddressAddPageRoutingModule } from './address-add-routing.module';

import { AddressAddPage } from './address-add.page';
import { GooglePlaceAutocompleteComponent } from 'src/app/components/google-place-autocomplete/google-place-autocomplete.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    AddressAddPageRoutingModule,
    TranslateModule
  ],
  declarations: [AddressAddPage, GooglePlaceAutocompleteComponent]
})
export class AddressAddPageModule {}
