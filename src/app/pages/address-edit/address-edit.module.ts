import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddressEditPageRoutingModule } from './address-edit-routing.module';

import { AddressEditPage } from './address-edit.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    AddressEditPageRoutingModule,
    TranslateModule
  ],
  declarations: [AddressEditPage]
})
export class AddressEditPageModule {}
