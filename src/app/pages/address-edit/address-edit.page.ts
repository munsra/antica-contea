import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { IonicComponentService } from 'src/app/services/ionic-component.service';
import { ShippingService } from 'src/app/services/shipping.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-address-edit',
  templateUrl: './address-edit.page.html',
  styleUrls: ['./address-edit.page.scss'],
})
export class AddressEditPage implements OnInit, OnDestroy {

  //public address: Observable<any[]>;
  //public addressArray: any = [];
  public addressId: string;
  public addressForm: FormGroup;

  places = [];

  placesSubscription: Subscription;
  private userProfileId: any;
  constructor(
    private authService: AuthService,
    public userService: UserService,
    public beerService: BeerService,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    public router: Router,
    private modalController: ModalController,
    private shippingService: ShippingService,
    private ionicComponentService: IonicComponentService,
    public formBuilder: FormBuilder
  ) {

    this.addressId = this.activatedRoute.snapshot.paramMap.get('addressId');
    this.addressForm = this.formBuilder.group({
      label: ['', Validators.required],
      fullname: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(6)]],
      address1: [''],
      address2: [''],
      city: ['', Validators.required],
      cap: ['', Validators.required],
      place: ['', Validators.required]
    });

    this.placesSubscription = this.shippingService.getPlaces().subscribe(places => {
      this.places = places.sort((a,b) => a.sigla.localeCompare(b.sigla));
    });

  }
  ngOnDestroy(): void {
    this.placesSubscription.unsubscribe();
  }

  ngOnInit() {

    this.userProfileId = this.authService.getUserId();

    //this.address = this.userService.getAddressById(this.addressId);
    this.userService.getAddressById(this.addressId).subscribe(res => {
      if (!res) {
        console.log("Address " + this.addressId + "not found.");
        return;
      }
      console.log("----->actionArray=" + res);
      console.log("Get user profile response=" + res);
      this.addressForm.get('label').setValue(res.label);
      this.addressForm.get('fullname').setValue(res.fullname);
      this.addressForm.get('phone').setValue(res.phone);
      this.addressForm.get('address1').setValue(res.address1);
      this.addressForm.get('address2').setValue(res.address2);
      this.addressForm.get('city').setValue(res.city);
      this.addressForm.get('cap').setValue(res.cap);
      //this.addressArray = res;
    });
  }
  async close() {
    await this.modalController.dismiss();
  }

  async editAddress() {
    // console.log("userProfileId="+this.userService.getUserId());
    console.log("_____call addAddress");
    if (!this.addressForm.valid) {
      console.log(this.addressForm.value);
      console.log("____addressForm invalid ")
      //this.presentAlert("invalid form");
    } else {
      console.log(this.addressForm.value);
      this.ionicComponentService.presentLoading();

      const editAddress = {
        uid: this.addressId,
        userProfileId: this.userProfileId,
        label: this.addressForm.value.label,
        fullname: this.addressForm.value.fullname,
        phone: this.addressForm.value.phone,
        address1: this.addressForm.value.address1,
        address2: this.addressForm.value.address2,
        city: this.addressForm.value.city,
        cap: this.addressForm.value.cap,
        place: this.addressForm.value.place
      };

      await this.userService.setAddress(editAddress)
        .then(() => {
          this.ionicComponentService.dismissLoading();
          this.close();
          //this.router.navigateByUrl('/food-address');
        }, (error) => {
          var errorMessage: string = error.message;
          console.log("ERROR:" + errorMessage);

          this.ionicComponentService.dismissLoading();
          this.ionicComponentService.presentToast(errorMessage, 4000);
          this.close();
        });

    }
  }

  async delAddress() {
    this.ionicComponentService.presentLoading();
    await this.userService.deleteAddress(this.addressId)
      .then(() => {
        // call loading 
        this.ionicComponentService.dismissLoading();
        this.close();
        //this.router.navigateByUrl('/food-address');

      }, (error) => {
        var errorMessage: string = error.message;
        console.log("ERROR:" + errorMessage);

        this.ionicComponentService.dismissLoading();
        this.ionicComponentService.presentToast(errorMessage, 4000);
        this.close();
      });
  }

}
