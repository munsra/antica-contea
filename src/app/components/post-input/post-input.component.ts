import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { AddNewPostComponent } from '../modals/add-new-post/add-new-post.component';

@Component({
  selector: 'app-post-input',
  templateUrl: './post-input.component.html',
  styleUrls: ['./post-input.component.scss'],
})
export class PostInputComponent implements OnInit {

  currentUser: User;
  
  constructor(private modalController: ModalController) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user')) as User;
  }

  async openNewPostModal(){
    const modal = await this.modalController.create({
      component: AddNewPostComponent
    });
    return await modal.present();
  }

}
