import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Message } from '../../models/message';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'src/app/models/user';
import { SharedService } from 'src/app/services/shared.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'chat-bubble',
  templateUrl: './chat-bubble.component.html',
  styleUrls: ['./chat-bubble.component.scss'],
  inputs: ['chatMessage'],
  encapsulation: ViewEncapsulation.None
})
export class ChatBubbleComponent implements OnInit {

  public chatMessage: Message;

  get minimalDatetime() {
    return this.utilService.getMinimalisticRelativeTime(this.chatMessage.dataCreation.toDate());
  }

  currentUser: User;
  constructor(private ngFireAuth: AngularFireAuth, 
    private utilService: UtilService,
    private sharedService: SharedService){
    
  };

  ionImgDidLoad(){
    this.sharedService.sendChatItemImageLoadedEvent();
  }

  ionImgWillLoad(){
    
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
  }
}
