import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { TextMaskModule } from 'angular2-text-mask';
import { CheckoutCreditCardPageRoutingModule } from '../pages/checkout-credit-card/checkout-credit-card-routing.module';
import { CreditCardComponent } from "./credit-card/credit-card.component";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        IonicModule,
        CheckoutCreditCardPageRoutingModule,
        TextMaskModule,
        TranslateModule
    ],
    declarations: [CreditCardComponent],
    exports: [CreditCardComponent]
})

export class ComponentsModule {}