import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Like } from 'src/app/models/like';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { FirebaseService } from 'src/app/services/firebase.service';
import { PostService } from 'src/app/services/post.service';
import { UtilService } from 'src/app/services/util.service';
import { PostCardMenuComponent } from '../modals/post-card-menu/post-card-menu.component';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss'],
})
export class PostCardComponent implements OnInit {
  
  currentUser: User;
  @Input() post: Post;

  get minimalDatetime() {
    return this.utilService.getMinimalisticRelativeTime(this.post.dataCreation.toDate());
  }

  constructor(private popoverController: PopoverController, 
              private postService: PostService,
              private utilService: UtilService,
              private fbService: FirebaseService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user')) as User;
  }

  presentPopover(user: User) {
    if(this.currentUser.uid == user.uid) 
      this.openPostCardMenuComponent();
    else 
      this.openUserMenuComponent(user);
  }

  async openUserMenuComponent(user: User){
    return;
    /*
    const popover = await this.popoverController.create({
      component: UsersMenuComponent,
      componentProps: {user: user},
      translucent: true,
    });
    return await popover.present();
    */
  }

  async openPostCardMenuComponent(){
    const popover = await this.popoverController.create({
      component: PostCardMenuComponent,
      componentProps: {post: this.post},
      translucent: true,
    });
    return await popover.present();
  }
  
  
}
