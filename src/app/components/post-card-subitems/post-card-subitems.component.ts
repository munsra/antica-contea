import { Component, Input, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Like } from 'src/app/models/like';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { FirebaseService } from 'src/app/services/firebase.service';
import { PostService } from 'src/app/services/post.service';
import { PostCommentCardComponent } from '../modals/post-comment-card/post-comment-card.component';

@Component({
  selector: 'app-post-card-subitems',
  templateUrl: './post-card-subitems.component.html',
  styleUrls: ['./post-card-subitems.component.scss'],
})
export class PostCardSubitemsComponent implements OnInit {

  @Input() post: Post;
  currentUser: User;

  constructor(private postService: PostService, 
              private fbService: FirebaseService,
              private modalController: ModalController) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user')) as User;
  }

  addOrRemoveLike(){
    if(!this.post.likes) this.post.likes = [];
    const index = this.post.likes.findIndex(x => x.userUid == this.currentUser.uid);
    if (index > -1) {
      const currentLike = this.post.likes[index];
      this.post.likes.splice(index, 1);
      this.postService.deleteLike(currentLike);
    }
    else{
      const newLike: Like = {
        uid: this.fbService.GeNewUid(),
        userUid: this.currentUser.uid,
        dataCreation: firebase.default.firestore.Timestamp.now(),
        modDatetime: firebase.default.firestore.Timestamp.now(),
        postUid: this.post.uid
      }
      this.post.likes.push(newLike);
      this.postService.setLike(newLike);
    }
    this.postService.setPost(this.post);
    
  }

  async openCommentPopup(){
    const popover = await this.modalController.create({
      component: PostCommentCardComponent,
      componentProps: {post: this.post},
    });
    return await popover.present();
  }

}
