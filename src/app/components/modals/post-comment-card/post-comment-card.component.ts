import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController, NavParams } from '@ionic/angular';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { Comment } from 'src/app/models/comment';
import { PostService } from 'src/app/services/post.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import * as firebase from 'firebase/app';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/services/shared.service';
import { NgZone } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-post-comment-card',
  templateUrl: './post-comment-card.component.html',
  styleUrls: ['./post-comment-card.component.scss'],
})
export class PostCommentCardComponent implements OnInit {

  currentUser: User;
  @Input() post: Post;
  @ViewChild(IonContent) content: IonContent;
  commentLoadSubscription: Subscription;

  /*
  public get comments(): Comment[] {
    return this.post.comments.sort((a, b) => {
      return b.dataCreation.seconds - a.dataCreation.seconds;
    });
  }
  */

  get minimalDatetime() {
    return this.utilService.getMinimalisticRelativeTime(this.post.dataCreation.toDate());
  }

  constructor(private modalController: ModalController,
    private navParams: NavParams,
    private postService: PostService,
    private zone: NgZone,
    private sharedService: SharedService,
    private utilService: UtilService,
    private fbService: FirebaseService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.post = this.navParams.data.post;
    this.commentLoadSubscription = this.sharedService.getCommentItemLoadEvent().subscribe(() => {
      this.scrollToBottom();
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

  scrollToBottom() {
    this.zone.run(() => {
      setTimeout(() => {
        this.content.scrollToBottom(300);
      }, 400);
    });
  }

}
