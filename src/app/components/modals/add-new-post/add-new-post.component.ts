import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { PostService } from 'src/app/services/post.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-add-new-post',
  templateUrl: './add-new-post.component.html',
  styleUrls: ['./add-new-post.component.scss'],
})
export class AddNewPostComponent implements OnInit {
  isSubmitted = false;
  currentDate: Date = null;
  currentUser: User;
  file: File;
  fileUrl: any;

  formGroup: FormGroup = new FormGroup({
    text: new FormControl(null, [Validators.required, Validators.minLength(2)]),
  })

  constructor(
    private modalController: ModalController,
    private postService: PostService,
    private authService: AuthService,
    private sharedService: SharedService,
    private fbService: FirebaseService) { }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
  }

  closeModal() {
    this.modalController.dismiss();
  }

  onFileChange(event) {
    this.file = event.target.files[0];

    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
    }
  }

  createPost() {
    this.isSubmitted = true;
    if (!this.formGroup.valid) {
      return false;
    }
    const text = this.formGroup.get('text').value;
    const user = {
      displayName: this.currentUser.displayName,
      uid: this.currentUser.uid,
      photoURL: this.currentUser.photoURL
    }
    const newPost: Post = {
      uid: this.fbService.GeNewUid(),
      mediaUrl: null,
      userUid: this.currentUser.uid,
      text: text,
      dataCreation: firebase.default.firestore.Timestamp.now(),
      modDatetime: firebase.default.firestore.Timestamp.now(),
      user: user,
      likes: []
    }
    if (this.fileUrl) {
      this.setPostWithFile(newPost, this.file);
    }else{
      this.postService.setPost(newPost).finally(() => {
        this.sharedService.sendPostItemLoadEvent();
        this.modalController.dismiss();
      });
    }
  }

  setPostWithFile(post, file){
    this.postService.uploadFileToPost(post, file).subscribe(newFile => {
      if (newFile) {
        newFile.ref.getDownloadURL().then(url => {
          post.mediaUrl = url;
          this.postService.setPost(post).finally(() => {
            this.sharedService.sendPostItemLoadEvent();
            this.modalController.dismiss();
          });
        }).catch(err => {
          console.log(err);
        });
      }
    });
  }

  get errorControl() {
    return this.formGroup.controls;
  }


}
