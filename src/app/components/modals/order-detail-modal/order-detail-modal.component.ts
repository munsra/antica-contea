import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { BeerService } from 'src/app/services/beer.service';
import { OrderService } from 'src/app/services/order.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-order-detail-modal',
  templateUrl: './order-detail-modal.component.html',
  styleUrls: ['./order-detail-modal.component.scss'],
})
export class OrderDetailModalComponent implements OnInit, OnDestroy {

  @Input() order;
  currentUser: User;
  address: any;
  addressSubscription: Subscription;
  userProfile: User;
  userProfileSubScription: Subscription;
  orderItems = [];

  total = 0;

  //**** Vat,Tax ****/
  vat: number = 10;
  vatTotal: number;

  //**** Delivery fee ****/
  fee: number = 15;
  feeTotal: number;
  valueTotal: number = 0;
  subTotal: any;
  grandTotal: any;

  constructor(
    private navParams: NavParams,
    private modalController: ModalController,
    private orderService: OrderService,
    private userService: UserService,
    private authService: AuthService
  ) { }

  async ngOnInit() {
    this.order = this.navParams.data.data;
    this.orderItems = this.order.orderItems;
    this.addressSubscription = this.userService.getAddressById(this.order.addressId).subscribe(address => {
      this.address = address;
    });

    this.userProfileSubScription = this.userService.getUserProfile(this.order.userProfileId).subscribe(userProfile => {
      this.userProfile = userProfile;
    });
    this.getCartProduct();
    this.currentUser = this.authService.getCurrentUser();
  }

  ngOnDestroy(): void {
    this.addressSubscription.unsubscribe();
    this.userProfileSubScription.unsubscribe();
  }

  getCartProduct() {
    // this.cartProducts = this.productService.getLocalCartProducts();
    const items = this.orderItems;
    console.log("page not refresh__________________food-cart : items=" + items);
    const selected = {};

    this.valueTotal = 0;

    for (const obj of items) {
       this.valueTotal += obj.price;
    }

    // all item in shopping cart
    this.orderItems = items;
    console.log("..............Order items =" + JSON.stringify(this.orderItems));
    this.total = this.valueTotal;
    this.subTotal = Number(this.total.toFixed(2));
    this.vatTotal = Number(
      (this.subTotal * this.vat / 100).toFixed(2)
    );
    this.feeTotal = Number((this.fee + this.vatTotal).toFixed(2));
    this.grandTotal = Number((this.subTotal + this.feeTotal).toFixed(2));
  }

  takeOrder(){
    this.orderService.takeOrder(this.currentUser, this.order);
    this.close();
  }

  close(){
    this.modalController.dismiss();
  }

}
