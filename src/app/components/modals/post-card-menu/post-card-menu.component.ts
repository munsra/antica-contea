import { Component, Input, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { PostService } from 'src/app/services/post.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-post-card-menu',
  templateUrl: './post-card-menu.component.html',
  styleUrls: ['./post-card-menu.component.scss'],
})
export class PostCardMenuComponent implements OnInit {

  currentUser: User;
  @Input() post: Post;

  constructor(private popoverController: PopoverController,
    private postService: PostService,
    private sharedService: SharedService,
    private navParams: NavParams) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.post = this.navParams.data.post;
  }

  deletePost(){
    this.postService.deletePost(this.post).finally(() => {
      this.sharedService.sendPostItemLoadEvent();
      this.closeModal();
    });
  }

  closeModal() {
    this.popoverController.dismiss();
  }

}
