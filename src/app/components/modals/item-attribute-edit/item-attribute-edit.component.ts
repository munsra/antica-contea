import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-item-attribute-edit',
  templateUrl: './item-attribute-edit.component.html',
  styleUrls: ['./item-attribute-edit.component.scss'],
})
export class ItemAttributeEditComponent implements OnInit {

  @Input() size: any;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  remove(){
    this.modalController.dismiss(
      {
        remove: true
      }
    )
  }

  save(){
    this.modalController.dismiss();
  }

  close(){
    this.modalController.dismiss();
  }
  

}
