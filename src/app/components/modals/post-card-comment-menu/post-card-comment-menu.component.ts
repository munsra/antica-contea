import { Component, Input, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { Comment } from 'src/app/models/comment';
import { PostService } from 'src/app/services/post.service';
import { SharedService } from 'src/app/services/shared.service';
import { Post } from 'src/app/models/post';

@Component({
  selector: 'app-post-card-comment-menu',
  templateUrl: './post-card-comment-menu.component.html',
  styleUrls: ['./post-card-comment-menu.component.scss'],
})
export class PostCardCommentMenuComponent implements OnInit {

  currentUser: User;
  @Input() comment: Comment;
  @Input() post: Post;

  constructor(private popoverController: PopoverController,
    private postService: PostService,
    private sharedService: SharedService,
    private navParams: NavParams) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.comment = this.navParams.data.comment;
    this.post = this.navParams.data.post;
  }

  deleteComment(){
    this.postService.deleteComment(this.comment).finally(() => {
      const index = this.post.comments.findIndex(x => x.uid == this.comment.uid);
      if (index > -1) {
        this.post.comments.splice(index, 1);
      }
      this.postService.setPost(this.post);
      this.sharedService.sendCommentItemLoadEvent();
      this.closeModal();
    });
  }

  closeModal() {
    this.popoverController.dismiss();
  }

}
