import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Chat } from 'src/app/models/chat';
import { Message } from 'src/app/models/message';
import { User } from 'src/app/models/user';
import { ChatService } from 'src/app/services/chat.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-chat-send-image',
  templateUrl: './chat-send-image.component.html',
  styleUrls: ['./chat-send-image.component.scss'],
})
export class ChatSendImageComponent implements OnInit {

  @Input() chat: Chat;
  @Input() user: User;
  @Input() message: string;
  @Input() file: File;
  fileUrl: any;


  constructor(
    private modalController: ModalController,
    private sharedService: SharedService,
    private chatService: ChatService) { }

  ngOnInit() {
    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
    }
  }

  closeModal() {
    const onClosedData: string = "no-data";
    this.modalController.dismiss(onClosedData);
  }

  sendChatImage() {
    this.sharedService.presentLoading('Sending message ...');
    this.chatService.uploadFileToChat(this.chat, this.file).toPromise().then(file => {
      if (file) {
        file.ref.getDownloadURL().then(url => {
          this.chatService.sendMessage(this.user, this.chat, this.message, url).finally(() => {
            this.sharedService.dismissLoading();
            this.modalController.dismiss();
          });
        }).catch(err => {
          console.log(err);
        });
      }
    });
  }
}
