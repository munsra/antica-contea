import { Component, Input, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { Comment } from 'src/app/models/comment';
import { FirebaseService } from 'src/app/services/firebase.service';
import { PostService } from 'src/app/services/post.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-post-comment-input',
  templateUrl: './post-comment-input.component.html',
  styleUrls: ['./post-comment-input.component.scss'],
})
export class PostCommentInputComponent implements OnInit {

  file: File;
  fileUrl: any;
  newComment = '';
  currentUser: User;
  @Input() post: Post;

  constructor(private fbService: FirebaseService,
              private postService: PostService,
              private sharedService: SharedService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
  }

  onFileChange(event) {
    this.file = event.target.files[0];
    var reader = new FileReader();

    reader.readAsDataURL(this.file); // read file as data url

    reader.onload = (event) => { // called once readAsDataURL is completed
      this.fileUrl = event.target.result;
    }
  }

  sendComment(){
    if(!this.post.comments) this.post.comments = [];
    const newComment: Comment = {
      uid: this.fbService.GeNewUid(),
      commentText: this.newComment,
      dataCreation: firebase.default.firestore.Timestamp.now(),
      modDatetime: firebase.default.firestore.Timestamp.now(),
      postUid: this.post.uid,
      userDisplayName: this.currentUser.displayName,
      userPhotoURL: this.currentUser.photoURL,
      userUid: this.currentUser.uid
    }
    
    if(this.fileUrl){
      this.setPostCommentWithFile(newComment, this.file);
    }else{
      this.postService.setComment(newComment);
      this.post.comments.push(newComment);
      this.postService.setPost(this.post);
      this.sharedService.sendCommentItemLoadEvent();
    }
    this.resetCommentInput();
    
  }

  setPostCommentWithFile(comment, file){
    this.postService.uploadFileToComment(comment, file).subscribe(newFile => {
      if (newFile) {
        newFile.ref.getDownloadURL().then(url => {
          comment.mediaUrl = url;
          this.postService.setComment(comment);
          this.post.comments.push(comment);
          this.postService.setPost(this.post);
          this.sharedService.sendCommentItemLoadEvent();
        }).catch(err => {
          console.log(err);
        });
      }
    });
  }

  resetCommentInput(){
    this.file = null;
    this.fileUrl = null;
    this.newComment = '';
  }

}
