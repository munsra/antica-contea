import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Comment } from 'src/app/models/comment';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { UtilService } from 'src/app/services/util.service';
import { PostCardCommentMenuComponent } from '../modals/post-card-comment-menu/post-card-comment-menu.component';

@Component({
  selector: 'app-post-comment-item',
  templateUrl: './post-comment-item.component.html',
  styleUrls: ['./post-comment-item.component.scss'],
})
export class PostCommentItemComponent implements OnInit {

  currentUser: User;
  @Input() comment: Comment;
  @Input() post: Post;

  get minimalDatetime(){
    return this.utilService.getMinimalisticRelativeTime(this.comment.dataCreation.toDate());
  }

  constructor(private popoverController: PopoverController,
              private utilService: UtilService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user')) as User;
  }

  async openCommentMenu(){
    if(this.comment.userUid == this.currentUser.uid){
      const popover = await this.popoverController.create({
        component: PostCardCommentMenuComponent,
        componentProps: {post: this.post, comment: this.comment},
        translucent: true,
      });
      return await popover.present();
    }
  }

  

}
