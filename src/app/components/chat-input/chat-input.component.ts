import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';
import { User } from '../../models/user';
import { Chat } from 'src/app/models/chat';
import { Message } from 'src/app/models/message';
import { ChatService } from 'src/app/services/chat.service';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ChatSendImageComponent } from '../modals/chat-send-image/chat-send-image.component';

@Component({
  selector: 'chat-input',
  templateUrl: './chat-input.component.html',
  inputs: ['chat'],
  styleUrls: ['./chat-input.component.scss'],
})
export class ChatInputComponent implements OnInit {

  @Input() chat: Chat;
  @Input() user: User;
  newMsg = '';
  file: File;
  downloadURL: Observable<string>;

  @ViewChild(IonContent) content: IonContent;

  constructor(private chatService: ChatService, private modalController: ModalController) { }

  ngOnInit() {

  }

  onFileChange(event) {
    this.file = event.target.files[0];
    this.openChatSendMessagePreview(this.file);
  }

  sendMessage() {
    this.chatService.sendMessage(this.user, this.chat, this.newMsg, null);
    this.newMsg = '';
  }

  async openChatSendMessagePreview(file: File) {
    const modal = await this.modalController.create({
      component: ChatSendImageComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        'chat': this.chat,
        'user': this.user,
        'message': this.newMsg,
        'file': file
      }
    });
    modal.present();
  }

}
