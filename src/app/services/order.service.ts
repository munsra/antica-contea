import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private beerOrdersCollection: AngularFirestoreCollection<any>;
  private beerOrders: Observable<any[]>;

  constructor(private datePipe: DatePipe,
    private afStore: AngularFirestore,
    private afStorage: AngularFireStorage,
    private fbService: FirebaseService) {
    this.beerOrdersCollection = afStore.collection('product_order');

    this.beerOrders = this.beerOrdersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getBeerOrders() {
    return this.beerOrders;
  }

  getOrdersByUsers(userUid){
    return this.afStore.collection('product_order', ref => ref.where('userProfileId', '==', userUid)).get();
  }

  //***********************//
  //****** Place order ******//
  //**********************//

  placeOrder(newOrder) {
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(newOrder.orderId).set(newOrder, options);
  }

  placeCreditCardOrder(newOrder) {
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(newOrder.orderId).set(newOrder, options);
  }

  takeOrder(currentUser: User, order) {
    const uptadedOrder = {
      userProfileId: order.userProfileId,
      userProfile: order.userProfile,
      addressId: order.addressId,
      orderId: order.orderId,
      paymentType: order.paymentType,
      orderItems: order.orderItems,
      status: "take",
      totalPrice: order.totalPrice,
      createdTime: order.createdTime,
      modDatetime: new Date(),
      userAdminUid: currentUser.uid,
      userAdminProfile: {
        userAdminUid: currentUser.uid,
        userAdminFirstname: currentUser.firstname,
        userAdminLastname: currentUser.lastname,
        userAdminEmail: currentUser.email
      },
      shipmentFee: order.shipmentFee
    }
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(order.orderId).set(uptadedOrder, options);
  }

  completeOrder(currentUser: User, order) {
    const uptadedOrder = {
      userProfileId: order.userProfileId,
      userProfile: order.userProfile,
      addressId: order.addressId,
      orderId: order.orderId,
      paymentType: order.paymentType,
      orderItems: order.orderItems,
      status: "completed",
      totalPrice: order.totalPrice,
      createdTime: order.createdTime,
      modDatetime: new Date(),
      userAdminUid: currentUser.uid,
      userAdminProfile: {
        userAdminUid: currentUser.uid,
        userAdminFirstname: currentUser.firstname,
        userAdminLastname: currentUser.lastname,
        userAdminEmail: currentUser.email
      },
      shipmentFee: order.shipmentFee
    }
    const options = { merge: true };
    return this.afStore.collection<any>('product_order').doc(order.orderId).set(uptadedOrder, options);
  }
}
