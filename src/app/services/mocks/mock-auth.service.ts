
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { NgZone } from '@angular/core';

export class MockAuthService {
  user: Observable<any>;

  constructor(
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,
    public router: Router,  
    public ngZone: NgZone 
  ) {
  }

  // Login in with email/password
  SignIn(email, password) {
    return '';
  }

  SignInAnonymously(){
    return '';
  }

  // Register user with email/password
  RegisterUser(email, password) {
    return '';
  }

  GetCurrentUser(){ 
    return '';
  }

  GetUser(userUid){ 
    return '';
  }

  get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    if(user){
      return (user.emailVerified !== false) ? true : false;
    }
    return false;
  }

  CreateGuestUser(username, password){
    return '';
  }

  // Email verification when new user register
  SendVerificationMail() {
    return '';
  }

  // Recover password
  PasswordRecover(passwordResetEmail) {
    return '';
  }

  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user') != null ? localStorage.getItem('user') : null);
    return (user !== null && (user.emailVerified || user.isAnonymous)) ? true : false;
  }

  // Sign in with Gmail
  GoogleAuth() {
    return '';
  }

  // Auth providers
  AuthLogin(provider) {
    return '';
  } 

  // Store user in localStorage
  SetUserData(user) {
    return '';
  }

  // Sign-out 
  SignOut() {
    return '';
  }

  

}