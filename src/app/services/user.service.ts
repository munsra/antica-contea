import { DatePipe } from '@angular/common';
import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Chat, ChatType } from '../models/chat';
import { User } from '../models/user';
import { AuthService } from './auth.service';
import { ChatService } from './chat.service';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersCollection: AngularFirestoreCollection<User>;
  private users: Observable<User[]>;
  private userId: string;
  private currentUser: User;

  constructor(public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    public firebaseService: FirebaseService,
    private chatService: ChatService,
    private authService: AuthService,
    private datePipe: DatePipe) {
    this.currentUser = this.getCurrentUser();
    this.usersCollection = afStore.collection('users');

    this.users = this.usersCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));
  }

  getUser(uid: string) {
    return this.afStore.collection('users').doc<User>(uid);
  }

  getCurrentUser(): User {
    return JSON.parse(localStorage.getItem('user')) as User;
  }

  setCurrentUser(currentUser: User) {
    localStorage.setItem('user', JSON.stringify(currentUser));
  }

  getUsers() {
    return this.users;
  }
  
  uploadUserImage(file) {
    const dateNow = this.datePipe.transform(new Date(Date.now()), 'yyyyMMddhhmmss');
    const filePath = '/users/' + this.currentUser.uid + '/photoUrl/' + dateNow;
    return this.firebaseService.UploadFile(filePath, file);
  }

  //*******************************//
  //******   user profile    ******//
  //*******************************//

  getUserProfile(userId) {
    console.log("userId=" + userId);
    console.log("getUserProfile");
    return this.afStore.doc<any>('users/' + userId).valueChanges();
  }


  async getUserProfileId() {
    const user = await this.authService.isLoggedIn()
    let userId = null;
    if (user) {
      // do something
      userId = await user.uid;
      //return   this.firestore.doc<any>('userProfile/'+this.userId).valueChanges();
    } else {
      // do something else
      console.log("++++++++No userId" + userId)
    }
    console.log("++++++++++getUserProfileId = " + userId)
    return userId;
  }

  updateUserProfile(
    userId: string,
    firstname: string,
    lastname: string,
    phone: string,
    email: string,
  ) {

    return this.afStore.doc<any>('users/' + userId).update({
      firstname: firstname,
      lastname: lastname,
      phone: phone,
      email: email
    });
  }

  //*******************************//
  //******   user address    ******//
  //*******************************//

  getAddressByUserId(userId) {
    console.log("_____getAddressByUserId=");
    return this.afStore.collection<any>('/userAddress', ref => ref
      .where('userProfileId', '==', userId))
      .snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            console.log("####get a group of countries=" + data);
            return { id, ...data };
          });
        })
      );
  }

  getAddressById(addressId: string) {
    console.log("_______getAddressById")
    return this.afStore.doc<any>('userAddress/' + addressId).valueChanges();
  }

  editAddress(
    addressId: string,
    title: string,
    fullname: string,
    phone: number,
    address: string) {
    console.log("addressId=" + addressId)
    return this.afStore.doc<any>('userAddress/' + addressId).update({
      label: title,
      fullname: fullname,
      phone: phone,
      address: address
    });
  }

  deleteAddress(addressId: string) {
    return this.afStore.doc('userAddress/' + addressId).delete();
  }

  setAddress(newAddress) {
    console.log("___addAddress=");
    return this.afStore.collection<any>('userAddress').doc(newAddress.uid).set(newAddress);
  }
}
