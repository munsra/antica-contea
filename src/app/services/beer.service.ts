import { Component, Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FirebaseService } from './firebase.service';


@Injectable({
  providedIn: 'root'
})
export class BeerService {

  //********* shopping cart array ************* */

  private cart = [];

  constructor(
    private firestore: AngularFirestore,
    private fireAuth: AngularFireAuth,
    private fbService: FirebaseService) {
  }

  //************************//
  //****** Items ******//
  //************************//

  getItems() {
    return this.firestore.collection<any>('/products', ref => ref.orderBy('sequence')).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          // get id from firebase metadata 
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getItemDetail(itemId: string) {
    return this.firestore.doc<any>('/products/' + itemId).valueChanges();
  }
  
  updateItem(item){
    return this.firestore.collection('/products').doc(item.uid).set(item);
  }

  removeItem(item){
    if(item.image){
      this.fbService.DeleteFile(item.image);
    }
    return this.firestore.collection('/products').doc(item.uid).delete();
  }

  //*******************//
  //****** Cart  ******//
  //*******************//

  getCart() {
    return this.cart;
  }

  addToCart(product) {
    //console.log("BEFORE PUSH___________FOODSERVICE addProduct()=" + JSON.stringify(product));
    this.cart.push(product);
    console.log("+AFTER PUSH _________________FOODSERVICE 3cart array=" + JSON.stringify(this.cart));
  }
  removeItemCart(index) {
    console.log("removeItemCart")
    this.cart.splice(index, 1);
    //update total price//

  }
  removeAllItemCart() {
    console.log("removeAllItemCart")
    this.cart = [];
    //this.cart.length = 0;
    //update total price//

  }

}
