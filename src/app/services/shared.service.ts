import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private subjectChatItem = new Subject<any>();
  private subjectDarkMode = new Subject<any>();
  private subjectPostItem = new Subject<any>(); 
  private subjectCommentItem = new Subject<any>(); 
  private subjectCommentRemoved = new Subject<any>(); 

  constructor(public loadingController: LoadingController) { }

  async presentLoading(message) {
    const loading = await this.loadingController.create({
      message: message,
      spinner: 'bubbles'
    });
    await loading.present();
  }

  async dismissLoading() {
    this.loadingController.dismiss();
  }

  sendChatItemImageLoadedEvent() {
    this.subjectChatItem.next();
  }

  getChatItemImageLoadEvent(): Observable<any> {
    return this.subjectChatItem.asObservable();
  }

  sendToogleDarkTheme(darkMode: boolean){
    this.subjectDarkMode.next(darkMode);
  }

  getToogleDarkTheme(){
    return this.subjectDarkMode.asObservable();
  }

  sendCommentItemLoadEvent(){
    this.subjectCommentItem.next();
  }

  getCommentItemLoadEvent(): Observable<any> {
    return this.subjectCommentItem.asObservable();
  }

  sendPostItemLoadEvent(){
    this.subjectPostItem.next();
  }

  getPostItemLoadEvent(): Observable<any> {
    return this.subjectPostItem.asObservable();
  }

  sendCommentRemovedEvent(){
    this.subjectCommentRemoved.next();
  }

  getCommentRemovedEvent(): Observable<any> {
    return this.subjectCommentRemoved.asObservable();
  }
}
