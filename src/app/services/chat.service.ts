import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { Chat, ChatType } from '../models/chat';
import { Message } from '../models/message';
import { User } from '../models/user';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private chatsCollection: AngularFirestoreCollection<Chat>;
  private chats: Observable<Chat[]>;

  constructor(
    private datePipe: DatePipe, 
    private afStore: AngularFirestore, 
    private afStorage: AngularFireStorage,
    private fbService: FirebaseService) {
    this.chatsCollection = afStore.collection('chats');

    this.chats = this.chatsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getChat(id: string) {
    return this.chatsCollection.doc(id).get();
  }

  getGroupChats() {
    return this.afStore.collection('chats', ref => ref.where('type', '==', 2)).snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as Chat;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        }));
  }

  getFriendChatsByUserUId(user: User) {
    const userToBeFiltered = {
      userDisplayName: user.displayName,
      userUid: user.uid,
    };
    return this.afStore.collection('chats', ref => ref.where("members", "array-contains", userToBeFiltered)
                                                      .where('type', '==', 1)).snapshotChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as Chat;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        }));
  }

  getMessagesFromChatId(id: string) {

    return this.chatsCollection.doc<Chat>(id).collection('messages', ref => ref.orderBy('dataCreation')).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Message;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));
  }

  setChatOnLocalStorage(chat: Chat) {
    localStorage.setItem('chat', JSON.stringify(chat));
  }

  addChat(newChat) {
    return this.chatsCollection.add(newChat);
  }

  setChatData(chat) {
    const options = { merge: true };
    return this.fbService.SetFirebaseCollection('chats', chat.uid, chat, options);
  }

  setFriendChat(currentUser, friend){
    const newChat: Chat = {
      uid: this.afStore.createId(),
      adminUid: currentUser.uid,
      dataCreation: firebase.default.firestore.Timestamp.now(),
      modDatetime: firebase.default.firestore.Timestamp.now(),
      type: ChatType.friend,
      members: [
        { userUid: currentUser.uid, userDisplayName: currentUser.displayName },
        { userUid: friend.uid, userDisplayName: friend.displayName },
      ]
    }

    return this.setChatData(newChat);
  }

  sendMessage(user: User, chat: Chat, messageTxt, mediaUrl) {
    const newMessage: Message = {
      uid: this.afStore.createId(),
      userUid: user.uid,
      username: user.displayName,
      chatUid: chat.uid,
      msgText: messageTxt,
      mediaUrl: mediaUrl,
      dataCreation: firebase.default.firestore.Timestamp.now(),
      modDatetime: firebase.default.firestore.Timestamp.now(),
    };

    this.chatsCollection.doc<Chat>(chat.uid).collection('messages').doc(newMessage.uid).set({
      messsageUid: newMessage.uid,
      userUid: newMessage.userUid,
      username: user.displayName,
      dataCreation: newMessage.dataCreation,
      msgText: newMessage.msgText,
      mediaUrl: mediaUrl
    }, { merge: true });

    const messageRef = this.afStore.collection('messages').doc(newMessage.uid);
    return messageRef.set(newMessage, {
      merge: true
    });
  }

  uploadFileToChat(chat: Chat, file: File) {
    const dateNow = this.datePipe.transform(new Date(Date.now()), 'yyyyMMddhhmmss');
    const filePath = '/chat/' + chat.uid + '/' + dateNow;
    return this.fbService.UploadFile(filePath, file);
  }
}
