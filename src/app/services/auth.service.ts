import { Injectable, NgZone } from '@angular/core';
import { User } from '../models/user'
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { first, map, switchMap } from 'rxjs/operators';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  user: Observable<any>;
  userId: string = "";
  userAuth: boolean = false;
  isUserAdmin: boolean = false;
  subjectUserAuthStateChanged = new Subject<any>();

  userSubscription: Subscription = new Subscription();


  constructor(
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,
    private firebaseService: FirebaseService,
    public router: Router,
    public ngZone: NgZone
  ) {
    this.ngFireAuth.onAuthStateChanged((user) => {
      if (user) {
        this.userId = user.uid;
        this.userAuth = true;
        this.user = this.afStore.doc<User>(`users/${user.uid}`).valueChanges();
        this.user.pipe(first()).toPromise().then(user => {
          if (user) {
            this.sendUserAuthStateChanged(user);
            this.isUserAdmin = user.isAdmin;
            localStorage.setItem('user', JSON.stringify(user));
          }
        });
      } else {
        this.user = of(null);
        this.sendUserAuthStateChanged(null);
      }
    });

  }

  // Login in with email/password
  signIn(email, password) {
    return this.firebaseService.SignIn(email, password)
  }

  signinUser(newEmail: string, newPassword: string): Promise<any> {
    return this.ngFireAuth.signInWithEmailAndPassword(newEmail, newPassword)
  }

  // register 
  signupUser(firstname: string, lastname: string, phone: string, username: string, password: string): Promise<any> {
    return this.ngFireAuth.createUserWithEmailAndPassword(username, password).then((user) => {
      const newUser: User = {
        uid: user.user.uid,
        email: user.user.email,
        firstname: firstname,
        lastname: lastname,
        displayName: firstname + " " + lastname,
        photoURL: null,
        emailVerified: user.user.emailVerified,
        isAnonymous: false,
        isAdmin: false,
        userSettings: User.setUserSettings(user.user.uid, false),
        phone: phone
      }
      localStorage.setItem('user', JSON.stringify(newUser));
      console.log("userid=========" + newUser.uid);   // firebase.database().ref('/userProfile').child(newUser.uid).set({
      this.afStore.collection('users').doc(newUser.uid).set(newUser);
    });
  }

  signInAnonymously() {
    return this.firebaseService.SignInAnonymously();
  }

  isLoggedIn(): Promise<any> {
    return this.ngFireAuth.authState.pipe(first()).toPromise();
  }

  isAdmin() {
    return this.isUserAdmin;
  }

  async getAuthState() {
    console.log("userService call getAuthState=" + this.userAuth);
    return await this.userAuth;
  }

  getUserId() {
    return this.userId;
  }

  // Register user with email/password
  registerUser(email, password) {
    return this.firebaseService.CreateUserWithEmailAndPassword(email, password)
  }

  observableCurrentUser(){
    if(this.getUserId()){
      this.userSubscription = this.firebaseService.GetFirebaseItem('users', this.getUserId()).valueChanges().subscribe(user => {
        console.log("currentUser")
        localStorage.setItem('user', JSON.stringify(user));
      })
    }
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  getUser(userUid) {
    return this.firebaseService.GetFirebaseItem('users', userUid).get();
  }

  get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      return (user.emailVerified !== false) ? true : false;
    }
    return false;
  }

  createGuestUser(username, password) {
    const uid = this.afStore.createId();
    const user = {
      photoURL: null,
      displayName: username,
      emailVerified: true,
      email: username,
      password: password,
      uid: uid
    };
    this.firebaseService.SetFirebaseCollection('users', uid, user);
  }

  // Email verification when new user register 
  sendVerificationMail() {
    this.firebaseService.GetAuthCurrentUser().then(res => {
      this.firebaseService.SendVerificationName(res).then(() => {
        this.router.navigate(['tabs/user-profile/verify-email']);
      });
    })

  }

  // Recover password
  passwordRecover(passwordResetEmail) {
    return this.firebaseService.SendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email has been sent, please check your inbox.');
      }).catch((error) => {
        window.alert(error)
      })
  }

  resetPassword(email: string): Promise<any> {
    return this.ngFireAuth.sendPasswordResetEmail(email);
  }

  // Sign in with Gmail
  googleAuth() {
    return this.firebaseService.GoogleAuth().then((result) => {
      this.ngZone.run(() => {
        this.setUserData(result.user).then(() => {
          this.router.navigate(['tabs']);
        });
      })
    }).catch((error) => {
      window.alert(error)
    });
  }

  // Store user in localStorage
  setUserData(user) {
    const userData = {
      uid: user.uid,
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      phone: user.phone,
      isAnonymous: user.isAnonymous,
      isAdmin: user.isAdmin,
      userSettings: user.userSettings,
      stripeId: user.stripeId
    }
    const options = { merge: true };
    return this.firebaseService.SetFirebaseCollection('users', user.uid, userData, options);
  }

  // Sign-out 
  signOut(): Promise<any> {
    localStorage.clear();
    this.user = of(null);
    if(this.userSubscription) this.userSubscription.unsubscribe()
    return this.ngFireAuth.signOut();
  }

  //*******************//
  //** Subscriptions  *//
  //*******************//

  sendUserAuthStateChanged(user) {
    this.subjectUserAuthStateChanged.next(user);
  }

  getUserAuthStateChangedvent(): Observable<any> {
    return this.subjectUserAuthStateChanged.asObservable();
  }
}