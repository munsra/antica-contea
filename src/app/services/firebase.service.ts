import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    private afStore: AngularFirestore,
    private ngFireAuth: AngularFireAuth,
    private afStorage: AngularFireStorage) { }


  SignIn(email, password) {
    return this.ngFireAuth.signInWithEmailAndPassword(email, password)
  }

  SignInAnonymously() {
    return this.ngFireAuth.signInAnonymously();
  }

  // Register user with email/password
  CreateUserWithEmailAndPassword(email, password) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password)
  }

  // Register user with email/password
  RegisterUser(email, password) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password)
  }

  SetFirebaseCollection(collectionName, uid, item, options?) {
    return this.afStore.collection(collectionName).doc(uid).set(item, options);
  }

  DeleteFirebaseItem(collectionName, uid) {
    return this.afStore.collection(collectionName).doc(uid).delete();
  }

  async DeleteFirebaseCollections(collection, where?, opStr?, value?) {
    if (where) {
      var collectionDeleteWhere = this.afStore.collection(collection).ref.where(where, opStr, value);
      collectionDeleteWhere.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.delete();
        });
      });
    } else {
      var collectionDelete = this.afStore.collection(collection);
      collectionDelete.get().forEach(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.delete();
        });
      });
    }
  }

  GetFirebaseItem(collectionName, uid) {
    return this.afStore.collection(collectionName).doc(uid);
  }

  GetFirebaseCollection(collectionName, where?, opStr?, value?) {
    return this.afStore.collection(collectionName).ref.where(where, opStr, value);
  }

  GetAuthCurrentUser() {
    return this.ngFireAuth.currentUser;
  }

  SendVerificationName(currentUser: firebase.User) {
    return currentUser.sendEmailVerification();
  }

  SendPasswordResetEmail(passwordResetEmail) {
    return this.ngFireAuth.sendPasswordResetEmail(passwordResetEmail);
  }

  GoogleAuth() {
    return this.AuthLogin(new firebase.auth.GoogleAuthProvider());
  }

  // Auth providers
  AuthLogin(provider) {
    return this.ngFireAuth.signInWithPopup(provider);
  }

  SignOut() {
    return this.ngFireAuth.signOut();
  }

  SetPersistantLocally() {
    return this.ngFireAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
  }

  GeNewUid() {
    return this.afStore.createId();
  }

  UploadFile(filePath: string, file: File) {
    const fileRef = this.afStorage.ref(filePath);
    const task = this.afStorage.upload(filePath, file).snapshotChanges();
    return task;
  }

  DeleteFile(fileUrl){
    this.afStorage.refFromURL(fileUrl).delete();
  }
}
