import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Post } from '../models/post';
import { FirebaseService } from './firebase.service';
import { DatePipe } from '@angular/common';
import { Like } from '../models/like';
import { Comment } from '../models/comment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private postsCollection: AngularFirestoreCollection<Post>;
  private posts: Observable<Post[]>;
  private currentUser: Post;

  constructor(private afStore: AngularFirestore,
    private datePipe: DatePipe,
    private fbService: FirebaseService) {
    this.postsCollection = afStore.collection<Post>('posts');

    this.posts = this.postsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      }));
  }

  getPosts() {
    return this.postsCollection;
  }

  setPost(post: Post) {
    return this.fbService.SetFirebaseCollection('posts', post.uid, post);
  }

  setLike(like: Like) {
    return this.fbService.SetFirebaseCollection('likes', like.uid, like);
  }

  deleteLike(like: Like) {
    return this.fbService.DeleteFirebaseItem('likes', like.uid);
  }

  setComment(comment: Comment) {
    return this.fbService.SetFirebaseCollection('comments', comment.uid, comment);
  }

  deleteComment(comment: Comment) {
    if(comment.mediaUrl){
      this.fbService.DeleteFile(comment.mediaUrl);
    }
    return this.fbService.DeleteFirebaseItem('comments', comment.uid);
  }

  async deletePost(post: Post) {
    var commentDeleteWhere = this.fbService.GetFirebaseCollection('comments', 'postUid', '==', post.uid);
    commentDeleteWhere.get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const comment = doc.data() as Comment;
          if(comment.mediaUrl){
            this.fbService.DeleteFile(comment.mediaUrl);
          }
          this.fbService.DeleteFirebaseItem('comments', comment.uid);
        });
      });

      var likeDeleteWhere = this.fbService.GetFirebaseCollection('likes', 'postUid', '==', post.uid);
      likeDeleteWhere.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          const like = doc.data() as Like;
          this.fbService.DeleteFirebaseItem('likes', like.uid);
        });
      });

    if(post.mediaUrl){
      this.fbService.DeleteFile(post.mediaUrl);
    }
    return this.fbService.DeleteFirebaseItem('posts', post.uid);
  }
  

  uploadFileToPost(post: Post, file: File) {
    const dateNow = this.datePipe.transform(new Date(Date.now()), 'yyyyMMddhhmmss');
    const filePath = '/post/' + post.uid + '/' + dateNow;
    return this.fbService.UploadFile(filePath, file);
  }

  uploadFileToComment(comment: Comment, file: File) {
    const dateNow = this.datePipe.transform(new Date(Date.now()), 'yyyyMMddhhmmss');
    const filePath = '/comment/' + comment.uid + '/' + dateNow;
    return this.fbService.UploadFile(filePath, file);
  }
}
