import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AuthVerifiedGuard } from "./guards/auth-verified.guard";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./pages/registration/registration.module').then( m => m.RegistrationPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'beer-detail/:itemId',
    loadChildren: () => import('./pages/beer-detail/beer-detail.module').then( m => m.BeerDetailPageModule)
  },
  {
    path: 'beer-detail-edit/:itemId',
    loadChildren: () => import('./pages/beer-detail-edit/beer-detail-edit.module').then( m => m.BeerDetailEditPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule),
  },
  {
    path: 'cart-checkout',
    loadChildren: () => import('./pages/cart-checkout/cart-checkout.module').then( m => m.CartCheckoutPageModule),
    canActivate: [AuthVerifiedGuard]
  },
  {
    path: 'address-edit',
    loadChildren: () => import('./pages/address-edit/address-edit.module').then( m => m.AddressEditPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'address-add',
    loadChildren: () => import('./pages/address-add/address-add.module').then( m => m.AddressAddPageModule),
    canActivate: [AuthGuard]
   //******** Prevent access to page with canActivate ******/
  },
  //**********************************************//
  //********* Firebase authentication ************//
  //**********************************************//

  {
    path: 'fire-signin',
    loadChildren: () => import('./firebase-auth/fire-signin/fire-signin.module').then( m => m.FireSigninPageModule)
  },
  {
    path: 'fire-signup',
    loadChildren: () => import('./firebase-auth/fire-signup/fire-signup.module').then( m => m.FireSignupPageModule)
  },
  {
    path: 'fire-forgot',
    loadChildren: () => import('./firebase-auth/fire-forgot/fire-forgot.module').then( m => m.FireForgotPageModule)
  },
  {
    path: 'checkout-credit-card',
    loadChildren: () => import('./pages/checkout-credit-card/checkout-credit-card.module').then( m => m.CheckoutCreditCardPageModule),
    canActivate: [AuthVerifiedGuard]
  },
  {
    path: 'cart-checkout-finish',
    loadChildren: () => import('./pages/cart-checkout-finish/cart-checkout-finish.module').then( m => m.CartCheckoutFinishPageModule),
    canActivate: [AuthVerifiedGuard]
  },
  {
    path: 'tabs/user-profile/profile-address',
    loadChildren: () => import('./firebase-auth/profile-address/profile-address.module').then( m => m.ProfileAddressPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'tabs/user-profile/profile-orders',
    loadChildren: () => import('./firebase-auth/profile-orders/profile-orders.module').then( m => m.ProfileOrdersPageModule),
    canActivate: [AuthVerifiedGuard]
  },
  {
    path: 'tabs/user-profile/profile-creditcards',
    loadChildren: () => import('./firebase-auth/profile-creditcards/profile-creditcards.module').then( m => m.ProfileCreditcardsPageModule),
    canActivate: [AuthVerifiedGuard]
  },
  {
    path: 'tabs/user-profile/verify-email',
    loadChildren: () => import('./firebase-auth/verify-email/verify-email.module').then( m => m.VerifyEmailPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'profile-order-detail',
    loadChildren: () => import('./firebase-auth/profile-order-detail/profile-order-detail.module').then( m => m.ProfileOrderDetailPageModule)
  },
  {
    path: 'tabs/user-profile/profile-about',
    loadChildren: () => import('./firebase-auth/profile-about/profile-about.module').then( m => m.ProfileAboutPageModule)
  },
  {
    path: 'tabs/user-profile/profile-privacy',
    loadChildren: () => import('./firebase-auth/profile-privacy/profile-privacy.module').then( m => m.ProfilePrivacyPageModule)
  },
  {
    path: 'new-credit-card',
    loadChildren: () => import('./pages/new-credit-card/new-credit-card.module').then( m => m.NewCreditCardPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
