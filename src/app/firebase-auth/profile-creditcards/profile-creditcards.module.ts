import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileCreditcardsPageRoutingModule } from './profile-creditcards-routing.module';

import { ProfileCreditcardsPage } from './profile-creditcards.page';
import { TranslateModule } from '@ngx-translate/core';
import { CheckoutCreditCardPageModule } from 'src/app/pages/checkout-credit-card/checkout-credit-card.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ProfileCreditcardsPageRoutingModule,
    CheckoutCreditCardPageModule
  ],
  declarations: [ProfileCreditcardsPage]
})
export class ProfileCreditcardsPageModule {}
