import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileCreditcardsPage } from './profile-creditcards.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileCreditcardsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileCreditcardsPageRoutingModule {}
