import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CreditCard } from 'src/app/models/card';
import { User } from 'src/app/models/user';
import { NewCreditCardPage } from 'src/app/pages/new-credit-card/new-credit-card.page';
import { AuthService } from 'src/app/services/auth.service';
import { IonicComponentService } from 'src/app/services/ionic-component.service';
import { StripeService } from 'src/app/services/stripe.service';

@Component({
  selector: 'app-profile-creditcards',
  templateUrl: './profile-creditcards.page.html',
  styleUrls: ['./profile-creditcards.page.scss'],
})
export class ProfileCreditcardsPage implements OnInit {

  cards: CreditCard[] = [];
  //**** User authentication  ****/
  userAuth: boolean = false; // Is user logged in ?
  userId: string;
  currentUser: User;

  constructor(
    private stripeService: StripeService,
    public authService: AuthService,
    private ionicComponentService: IonicComponentService,
    private modalController: ModalController
  ) { 
    
  }

  ngOnInit() {

  }

  async ionViewWillEnter() {

    this.userAuth = await this.authService.isLoggedIn();
    this.currentUser = this.authService.getCurrentUser();

    this.userId = this.authService.getUserId();

    this.getAllCards();
  }

  getAllCards(){
    this.ionicComponentService.presentLoading();

    var getAllCardsBoody = {
      custId: this.currentUser.stripeId
    };

    this.stripeService.getAllCard(getAllCardsBoody).subscribe((cards: any) => {
      this.cards = [];
      console.log(cards.data);
      if(cards.data){
        cards.data.forEach((card) => {
          // doc.data() is never undefined for query doc snapshots
          const itemCard: CreditCard = {
            stripeId: card.id,
            cardNumber: "**** **** **** " + card.last4,
            expires: new Date(card.exp_year + "-"+card.exp_month),
            ccv: null,
            brand: card.brand,
            type: null,
          };
          this.cards.push(itemCard);
        });
      }
      
      this.ionicComponentService.dismissLoading();
      console.log(this.cards);
    });
  }

  deleteCard(card: CreditCard){
    this.ionicComponentService.presentLoading();
    const body = {
      userId: this.currentUser.stripeId,
      cardId: card.stripeId
    }
    this.stripeService.deleteCard(body).subscribe(res => {
      this.ionicComponentService.dismissLoading();
      this.getAllCards();
    })
  }

  async openCreditCardModal(){
    console.log("openModal");
    const modal = await this.modalController.create({
      component: NewCreditCardPage
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        if(dataReturned.data.stripeId){
          this.getAllCards();
        }
      }
    })

    return await modal.present();
  }

}
