import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePrivacyPageRoutingModule } from './profile-privacy-routing.module';

import { ProfilePrivacyPage } from './profile-privacy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePrivacyPageRoutingModule
  ],
  declarations: [ProfilePrivacyPage]
})
export class ProfilePrivacyPageModule {}
