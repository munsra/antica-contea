import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePrivacyPage } from './profile-privacy.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilePrivacyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePrivacyPageRoutingModule {}
