import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileAddressPageRoutingModule } from './profile-address-routing.module';

import { ProfileAddressPage } from './profile-address.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ProfileAddressPageRoutingModule
  ],
  declarations: [ProfileAddressPage]
})
export class ProfileAddressPageModule {}
