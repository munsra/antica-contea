import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { AddressAddPage } from '../../pages/address-add/address-add.page';
import { AddressEditPage } from '../../pages/address-edit/address-edit.page';

@Component({
  selector: 'app-profile-address',
  templateUrl: './profile-address.page.html',
  styleUrls: ['./profile-address.page.scss'],
})
export class ProfileAddressPage implements OnInit {

  //**** Address ****/
  public addresses = [];
  public selectAddressId: string;
  //**** User authentication  ****/
  userAuth: boolean = false; // Is user logged in ?
  userId: string;
  currentUser: User;

  private currentUserSubscription: Subscription;
  private addressesSubscription: Subscription;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
    
  }

  async ionViewWillEnter() {
    this.userAuth = await this.authService.isLoggedIn();
    this.currentUser = this.authService.getCurrentUser();

    this.userId = this.authService.getUserId();
    this.currentUserSubscription = this.authService.getUserAuthStateChangedvent().subscribe(user => {
      this.currentUser = user;
    });

    this.addressesSubscription = this.userService.getAddressByUserId(this.userId).subscribe(addresses => {
      this.addresses = addresses;
    })
  }

  addressGroupChange(event) {
    console.log("addressGroupChange", event.detail);
    //this.selectedRadioGroup = event.detail;
    this.selectAddressId = event.detail.value;
    console.log("selectAddressId=" + this.selectAddressId);
    var currentAddress = this.addresses.filter(x => x.id == this.selectAddressId)[0];
  }

  async editAddressModal(addressId: string) {
    console.log("openModal");
    const modal = await this.modalController.create({
      component: AddressEditPage,
      componentProps: {
        addressId: addressId
      }
    });
    return await modal.present();
  }

  async openAddressModal() {
    console.log("openModal");
    const modal = await this.modalController.create({
      component: AddressAddPage,
      componentProps: {
      }
    });
    return await modal.present();
  }

}
