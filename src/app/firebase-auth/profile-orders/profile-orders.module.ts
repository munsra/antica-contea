import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileOrdersPageRoutingModule } from './profile-orders-routing.module';

import { ProfileOrdersPage } from './profile-orders.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    ProfileOrdersPageRoutingModule
  ],
  declarations: [ProfileOrdersPage]
})
export class ProfileOrdersPageModule {}
