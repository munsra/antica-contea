import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileOrdersPage } from './profile-orders.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileOrdersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileOrdersPageRoutingModule {}
