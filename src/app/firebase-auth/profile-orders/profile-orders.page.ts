import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { OrderService } from 'src/app/services/order.service';
import { UserService } from 'src/app/services/user.service';
import { ProfileOrderDetailPage } from '../profile-order-detail/profile-order-detail.page';

@Component({
  selector: 'app-profile-orders',
  templateUrl: './profile-orders.page.html',
  styleUrls: ['./profile-orders.page.scss'],
})
export class ProfileOrdersPage implements OnInit {

  orders = [];

  //**** User authentication  ****/
  userAuth: boolean = false; // Is user logged in ?
  userId: string;
  currentUser: User;

  private currentUserSubscription: Subscription;
  private ordersSubscription: Subscription;

  constructor(
    private orderService: OrderService,
    public authService: AuthService,
    private userService: UserService,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.userAuth = await this.authService.isLoggedIn();
    this.currentUser = this.authService.getCurrentUser();

    this.userId = this.authService.getUserId();
    this.currentUserSubscription = this.authService.getUserAuthStateChangedvent().subscribe(user => {
      this.currentUser = user;
    });

    this.ordersSubscription = this.orderService.getOrdersByUsers(this.userId).subscribe(orders => {
      this.orders = [];
      orders.forEach((doc) => {
        // doc.data() is never undefined for query doc snapshots
        this.orders.push(doc.data());
      });
      this.orders.sort((a, b) => (a.createdTime.seconds < b.createdTime.seconds) ? 1 : -1);
      console.log(this.orders);
    })
  }

  openOrderDetail(order){
    this.modalController.create({
      component: ProfileOrderDetailPage,
      cssClass: 'fullscreen-modal',
      componentProps: {
        order: order
        // index: image
      },
      swipeToClose: true,
    }).then(modal => {
      modal.present();
    });
  }

}
