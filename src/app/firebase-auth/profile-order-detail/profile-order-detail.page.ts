import { Component, Input, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-profile-order-detail',
  templateUrl: './profile-order-detail.page.html',
  styleUrls: ['./profile-order-detail.page.scss'],
})
export class ProfileOrderDetailPage implements OnInit {

  @Input() order: any;

  constructor(
    private navParams: NavParams, 
    private modalController: ModalController
  ) { }

  ngOnInit() {
    console.log("order");
    console.log(this.order);
  }

  close(){
    this.modalController.dismiss();
  }

}
