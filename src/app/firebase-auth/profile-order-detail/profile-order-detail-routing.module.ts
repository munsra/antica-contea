import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileOrderDetailPage } from './profile-order-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileOrderDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileOrderDetailPageRoutingModule {}
