import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileOrderDetailPageRoutingModule } from './profile-order-detail-routing.module';

import { ProfileOrderDetailPage } from './profile-order-detail.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileOrderDetailPageRoutingModule,
    TranslateModule
  ],
  declarations: [ProfileOrderDetailPage]
})
export class ProfileOrderDetailPageModule {}
