import { Like } from "./like";
import {Comment} from 'src/app/models/comment';

export interface Post {
    uid: string;
    userUid: string;
    text: string;
    mediaUrl?: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
    user: any;
    comments?: Comment[];
    likes: Like[];
}