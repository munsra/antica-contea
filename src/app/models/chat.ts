import { Message } from './message';
export interface Chat {
    uid: string;
    adminUid: string;
    title?: string;
    lastMsg?: string;
    chatMedia?: string;
    lastUsername?: string,
    dataCreation:  firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
    type: number;
    messages?: Message[];
    members?: any[];
}

export enum ChatType{
    friend = 1,
    group = 2,
}