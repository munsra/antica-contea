export interface CreditCard {
    stripeId: string;
    cardNumber: string;
    expires: Date,
    ccv: number,
    brand: string,
    type: string,
    isValid?: boolean,
    errorMessage?: string
}