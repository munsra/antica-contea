export interface Like {
    uid: string;
    userUid: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
    postUid: string;
}