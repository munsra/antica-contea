export interface Message {
    uid: string;
    userUid: string;
    username: string,
    chatUid: string,
    msgText: string;
    mediaUrl?: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
}