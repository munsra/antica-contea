export interface Comment {
    uid: string;
    userUid: string;
    userDisplayName: string,
    postUid: string,
    userPhotoURL: string,
    commentText: string;
    mediaUrl?: string;
    dataCreation: firebase.default.firestore.Timestamp;
    modDatetime: firebase.default.firestore.Timestamp;
}